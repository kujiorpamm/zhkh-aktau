<?php

namespace app\assets;

use yii\web\AssetBundle;

class TimeAsset extends AssetBundle
{
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

	public $js = [
	    'https://momentjs.com/downloads/moment-with-locales.js',
	];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
