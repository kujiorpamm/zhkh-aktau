<?php

namespace app\components;
use yii\base\BootstrapInterface;
use Yii;

class LanguageSelector implements BootstrapInterface
{
    public $supportedLanguages = [];

    public function bootstrap($app)
    {

		switch(Yii::$app->session['lang']){
			case 'ru':
				Yii::$app->language = "ru";
				break;
			case 'kk':
				Yii::$app->language = "kk";
				break;
			
			default:
				Yii::$app->language = "ru";
				break;
		}
    }
}

?>