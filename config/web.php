<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'app\components\LanguageSelector'],
    'language' => 'ru',
    'sourceLanguage' => 'ru',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'IMLPgnvBY7RlSblbml2O6vtX60x0XqMK',
            'baseUrl' => '',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'formatter' => [
            'nullDisplay' => '',
            'defaultTimeZone' => 'Asia/Aqtau'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            	//'<controller>/<action>' => '<controller>/<action>',
            	'login' => 'site/login',
            	'report' => 'application/default/new-application',
            	'registration' => 'user/default/registration',
            	'me' => 'user/default/profile',
            	'me/reset-password' => 'user/default/reset-password',
            	'activate' => 'user/default/activate',
            	'archive' => 'workplace/default/archive',
            	'event-map' => 'site/event-map',
            	'project-map' => 'site/project-map',
            	'maek-dashboard' => 'site/maek-dashboard',
            	'infographics-ouok' => 'site/infographics-ouok',
            	'/news/<id:\d+>' => '/news/default/view',
            	'/application/<id:\d+>' => '/application/default/view',
            	'/organizations/<id:\d+>' => '/organization/default/view',
            	'/organizations/reports/<id:\d+>' => '/organization/default/view-report',
            	'/organizations' => '/organization',
            	'/about' => '/site/about',
            	'/api/export/<table_name:\w+>' => '/api/export',
            	//'/api/export/<table_name:\w+>/<date:\d{4}-\d{2}-\d{2}>' => '/api/export',
            	'/api/export/<table_name:\w+>/<date:[0-9\-]+>' => '/api/export',
            	'/stats' => '/statistics/default/stats',
            	'/stats-uncompleted' => '/statistics/default/stats-uncompleted',
            	'/statistics/uncompleted' => '/statistics/default/uncompleted',
            ],
        ],

	    'assetManager' => [
	        'converter'=> [
	            'class'=>'nizsheanez\assetConverter\Converter',
	            'force'=>false,
	            'destinationDir' => 'compiled',
	            'parsers' => [
					'less' => [ // file extension to parse
						'class' => 'nizsheanez\assetConverter\Less',
						'output' => 'css', // parsed output file type
						'options' => array(
							'auto' => true, // optional options
						)
					]
				]
	        ]
	    ],
	    'i18n' => [
        'translations' => [
            'app*' => [
                'class' => 'yii\i18n\DbMessageSource',
                'sourceMessageTable'=>'{{%source_message}}',
                'messageTable'=>'{{%message}}'
            ],
        ],
    ],

    ],

    'modules' => [
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'defaultRoute' => '@webroot/uploads',
            'uploadDir' => '@webroot/uploads',
            'fileAllowExtensions' => ['doc', 'docx', 'pdf'],
            'imageAllowExtensions' => ['jpg', 'png', 'gif', 'bmp', 'svg'],
        ],
        'user' => [
            'class' => 'app\modules\user\Module',
        ],
        'organization' => [
            'class' => 'app\modules\organization\module',
        ],
        'application' => [
            'class' => 'app\modules\application\module',
        ],
        'workplace' => [
            'class' => 'app\modules\workplace\module',
        ],
        'gridview' =>  [
        	'class' => '\kartik\grid\Module'
        ],
        'news' => [
            'class' => 'app\modules\news\module',
        ],
        'dashboard' => [
            'class' => 'app\modules\dashboard\module',
        ],
        'statistics' => [
            'class' => 'app\modules\statistics\module',
        ],
        'notification' => [
            'class' => 'app\modules\notification\Module',
        ],
        'api' => [
            'class' => 'app\modules\api\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['37.99.45.170', '::1'],
    ];
}

return $config;
