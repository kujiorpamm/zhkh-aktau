<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\modules\application\models\Application;
use app\modules\application\models\ApplicationTypes;
use app\modules\organization\models\Organization;
use app\modules\organization\models\OrganizationContract;
use app\modules\organization\models\Addresses;

class AjaxController extends Controller {
	
	
	public function behaviors()
    {
        return [
            
        ];
    }
    
    public function actionApplicationSubcategories() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		
		if($_POST['id'] && is_numeric($_POST['id'])) {
			Yii::$app->response->statusCode = 200;
			return ApplicationTypes::getSubTypesAsArray($_POST['id']);
		} else {
			Yii::$app->response->statusCode = 500;
			return;
		}
	}
	
	public function actionOrganizationContracts() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$return = OrganizationContract::find()->where(['=', 'organization_id', $_POST['id']])->select('name')->indexBy('id')->column();
		
		if($return) {
			Yii::$app->response->statusCode = 200;
			return $return;
		} else {
			Yii::$app->response->statusCode = 500;
			return;
		}		
		
	}
	
	public function actionSearchOrganization() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		
		$addr_id = Yii::$app->request->post('id');
		$address = Addresses::findOne(['id'=>$addr_id]);
		$org = Organization::findOne(['id'=>$address->organization_id]);
		if($address && $org) {
			Yii::$app->response->statusCode = 200;
			
			$link = Yii::t('app', 'Ваша управляющая организация: {0}', ["<a href='/organizations/{$org->id}'>{$org->name}</a>"]);
			return ['link'=>$link];
		}
		Yii::$app->response->statusCode = 500;
		return ['link'=>Yii::t('app', 'Мы не нашли организацию, которая обслуживает этот дом...')];
	}
	
}