<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\modules\application\models\Application;
use app\modules\application\models\ApplicationAssignment;
use app\modules\application\models\ApplicationTypes;
use app\modules\news\models\News;
use app\modules\organization\models\Addresses;
use app\modules\dashboard\models\Dashboard;
use app\modules\organization\models\OrganizationProjects;
use yii\redactor\RedactorModule;

class ApiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'export' => ['post'],
                ],
            ],
        ];
    }
	
	public function actionExport($table_name, $date=null) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$tables = [
			'application' => Application::find()->select(['id', 'application_type', 'date_created', 'date_plane_end', 'date_end', 'coords', 'seriosness', 'status', 'address', 'last_updated']),
			'application_assignment' => ApplicationAssignment::find()->select(['id', 'application_id', 'organization_id', 'last_updated']),
			'application_types' => ApplicationTypes::find(),
			'organization' => ApplicationTypes::find(),
			'addresses' => Addresses::find(),
		];
		
		$headers = apache_request_headers();
		if($_POST['token'] !== "9b1ca857a76266b6fa347ed368db2eb2ad4cc69c7019e6dc7c2c295ac796e107") {
			\Yii::$app->response->setStatusCode(403);
			return ['status' => 403, 'message' => "Доступ запрещен"];	
		}
		
		if(!$tables[$table_name]) {
			\Yii::$app->response->setStatusCode(500);
			return ['status' => 500, 'message' => 'Такой таблицы не существует'];
		}
		
		$result = ($date) ? $tables[$table_name]->where(['>=', 'last_updated', $date])->all() : $tables[$table_name]->all();
		return $result;
	}
	
	public function beforeAction($action) {            
	    if (in_array($action->id, ['export'])) {
	        $this->enableCsrfValidation = false;
	    }

	    return parent::beforeAction($action);
	}
    
}
