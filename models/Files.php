<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property integer $table_name
 * @property integer $table_id
 * @property string $path
 * @property string $ext
 * @property string $title_ru
 * @property string $title_kz
 */
class Files extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_name', 'table_id', 'path', 'ext', 'title_ru', 'title_kz'], 'required'],
            [['title_ru', 'title_kz', 'file'], 'required', 'on' => 'create'],

            [['path'], 'string', 'max' => 256],
            [['table_name'], 'string', 'max' => 64],
            [['ext'], 'string', 'max' => 32],
            [['title_ru', 'title_kz'], 'string', 'max' => 512],
            [['file'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'table_name' => Yii::t('app', 'Table Name'),
            'table_id' => Yii::t('app', 'Table ID'),
            'path' => Yii::t('app', 'Path'),
            'ext' => Yii::t('app', 'Ext'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_kz' => Yii::t('app', 'Title Kz'),
        ];
    }

    public function prepare() {
      if(!$this->file) throw new \Exception("Ошибка в модели, не указан файл", 1);
      $this->ext = $this->file->extension;
      $this->path = '/uploads/table_based/' . $this->file->baseName . rand() . '.' . $this->file->extension;
      try {
        $this->file->saveAs(substr($this->path, 1));
      } catch (\Exception $e) {
        var_dump($e); exit;
      }
    }

    public function getTitle() {
      if(Yii::$app->language == 'ru') {
        return $this->title_ru;
      } else if (Yii::$app->language == 'kk' && $this->title_kz) {
        return $this->title_kz;
      }
      return $this->title_ru;
    }

}
