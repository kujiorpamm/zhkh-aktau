<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Json;

use app\modules\user\models\User;
use app\modules\application\models\Application;
use app\modules\application\models\ApplicationTypes;
use app\modules\application\models\ApplicationAssignment;
use app\modules\application\models\ApplicationCommends;
use app\modules\application\models\ApplicationFiles;


class ApplicationController extends Controller
{

  public $post_data;

  public function actionGetTypes() {
    return ApplicationTypes::find()->select(['id', 'name_ru', 'name_kz'])->where(['=', 'parent_id', 1])->all();
  }

  public function actionCreate() {
    $_data = $this->post_data;

    $model = new Application;
    foreach ($_data as $key => $value) {
      if($model->hasAttribute($key)) $model[$key] = $value;
    }

    if(!$_data->author && !($_data->applicant_phone && $_data->applicant_addr  && $_data->applicant_name)) {
      $model->addError('author', 'Если не указан ID пользователя, то нужно указать поля applicant_phone, applicant_addr, applicant_name');
      return ['status'=>0, 'error'=>$model->getErrors()];
    }

    if(!$_data->image) {
      $model->addError('image', 'Нужно указать фотографию (base64 JPG оптимизированный по размеру)');
      return ['status'=>0, 'error'=>$model->getErrors()];
    }

    if(!$_data->author) $model->author = 2;

    $model->date_created = date('Y-m-d H:i:s');
		$model->date_plane_end = date('Y-m-d H:i:s', strtotime(Yii::$app->params['application']['days_to_add']));
		$model->active = 1;
		$model->status = 1;

    if($model->save()) {

      $_data->image = str_replace('data:image/jpeg;base64,', '', $_data->image);
      $_data->image = str_replace(' ', '+', $_data->image);
      $data = base64_decode($_data->image);
      $file = "images/applications/" . $model->id  . '.jpeg';
      $success = file_put_contents($file, $data);

      $file = new ApplicationFiles;
      $file->type = 1;
      $file->ext = 'jpeg';
      $file->src = "/images/applications/" . $model->id  . '.jpeg';
      $file->application_id = $model->id;
      $file->save();

      return ['status'=>1, 'id'=>$model->id];
    } else {
      return ['status'=>0, 'error'=>$model->getErrors()];
    }
    return ['status'=>0];
  }

  public function actionUpdate() {
    $_data = $this->post_data;
    $model = Application::findOne(['id'=>$_data->id]);
    foreach ($_data as $key => $value) {
      if($model->hasAttribute($key)) $model[$key] = $value;
    }

    if($_data->image) {
      $_data->image = str_replace('data:image/jpeg;base64,', '', $_data->image);
      $_data->image = str_replace(' ', '+', $_data->image);
      $data = base64_decode($_data->image);
      $file = "images/applications/" . $model->id  . '.jpeg';
      $success = file_put_contents($file, $data);

      $file = new ApplicationFiles;
      $file->type = 1;
      $file->ext = 'jpeg';
      $file->src = "/images/applications/" . $model->id  . '.jpeg';
      $file->application_id = $model->id;
      $file->save();
    }

    if($model->save()) {
      return ['status'=>1];
    } else {
      return ['status'=>0, 'error'=>$model->getErrors()];
    }

  }

  public function actionView() {
    $_data = $this->post_data;
    $_export = [];
    $model = Application::find()->where(['id'=>$_data->id])->one();
    // $_export = array_merge($_export, $model);
    foreach ($model->assignments as $assignment) {
      $assignments[] = [
        'id' => $assignment->organization_id,
        'name' => $assignment->organization->name
      ];
    }
    foreach ($model->commends as $commend) {
      $commends[] = [
        'date' => $commend->date,
        'text' => $commend->text,
        'author' => $commend->author->fullname,
        'organization' => $commend->author->organization->name
      ];
    }

    return ['application'=>$model, 'assignments'=>$assignments, 'commends'=>$commends, 'image_before'=>$model->imageBefore];
  }

  public function beforeAction($action) {
      Yii::$app->controller->enableCsrfValidation = false;
      Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

      $this->post_data = (object) Yii::$app->request->bodyParams;

	    return parent::beforeAction($action);
	}

  function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }

}
