<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;

use app\modules\user\models\User;


class UserController extends Controller
{

  public $post_data;

  public function actionCreate() {

    $_data = $this->post_data;

    if($model = User::findOne(['email'=>$_data->email])) {
      return ['status'=>1, 'id'=>$model->id];
    }

    $model = new User;
  	$model->scenario = "registration";

    foreach ($_data as $key => $value) {
      $model[$key] = $value;
    }

    $model->password = md5($this->generateRandomString(5));
    $model->authkey = $model->password;
    $model->agreement = 1;

    if($model->save()) {
      return ['status'=>1, 'id'=>$model->id];
    } else {
      return ['status'=>1, 'error'=>$model->getErrors()];
    }

    return ['status'=>0];
  }

  public function actionUpdate() {
    $_data = $this->post_data;
    if($model = User::findOne(['id'=>$_data->user_id])) {
      foreach ($_data as $key => $value) {
        if(isset($model[$key])) $model[$key] = $value;
      }
      if($model->save()) {
        return ['status'=>1, 'id'=>$model->id];
      } else {
        return ['status'=>1, 'error'=>$model->getErrors()];
      }
    }
    return ['status'=>0];
  }

  public function beforeAction($action) {
      Yii::$app->controller->enableCsrfValidation = false;
      Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

      $this->post_data = (object) Yii::$app->request->bodyParams;

	    return parent::beforeAction($action);
	}

  function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }

}
