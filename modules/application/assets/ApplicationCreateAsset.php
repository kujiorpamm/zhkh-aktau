<?php
namespace app\modules\application\assets;

use yii\web\AssetBundle;

class ApplicationCreateAsset extends AssetBundle {
	
	public $sourcePath = __DIR__ ;
	public $publishOptions = [
	    'forceCopy' => true,
	];
	public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
	
	public $js = [
		'https://api-maps.yandex.ru/2.1/?lang=ru_RU&load=Map,Placemark',
		'js/application_form.js'
	];
	
	public $depends = [ 
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset', 
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset', 
    ];  
	
}

?>