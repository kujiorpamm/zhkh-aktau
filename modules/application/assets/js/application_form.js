
var ymap;
var placemark;

$(document).ready(function(){
	
	$('#app_main_cat').change(function(e) {
		$(this).main_category_change();
	});
	
	
	if($('#ymap_view').length > 0) {
		ymaps.ready(init_view_map);
	} else {
		ymaps.ready(init_create_map);
	}
		
	$('#app_form').on('afterValidate', function (event, messages) {
	    if(typeof $('.has-error').first().offset() !== 'undefined') {
	        $('html, body').animate({
	            scrollTop: $('.has-error').first().offset().top - 150
	        }, 1000);
	    }
	});
	
	
	if(typeof is_view !== 'undefined') {
		init_view();
	}
	
});


//Вызывается только на странице View
function init_view() {
	$('#applicationassignment-organization_id').change(function(){
		$.ajax({
			url: "/ajax/organization-contracts",
			type: "POST",
			dataType: JSON,
			data: {
				id: $('#applicationassignment-organization_id').val()
			},
			complete: function(data) {
				var result = $.parseJSON(data.responseText);
				var new_data = '<option value>Выберите контракт </option>';;
				$.each(result, function(k, v) {
					new_data += '<option value='+k+'>'+v+'</option>';	
				});
				
				$('#applicationassignment-contract_id').html(new_data);
				$('.application_contract_id_container').show();
			},
			error: function(data) {
				$('.application_contract_id_container').hide();				
				$('#applicationassignment-contract_id').html("");
			},
			beforeSend: function() {
				$('.application_contract_id_container').hide();
				$('#applicationassignment-contract_id').html("");
				$('#applicationassignment-contract_id').val("");
			}	
		});	
	});
	
	
	if($('#application-status').val() == 3) {
			$(".image-loader").show();
		} else {			
			$(".image-loader").hide();
		}
	
	$('#application-status').change(function(e) {
		
		if($(this).val() == 3) {
			$(".image-loader").show();
		} else {			
			$(".image-loader").hide();
		}
		
	});
	
}

function init_create_map(){     
    
    //Грузить просто карту, когда создают заявку
    if(typeof app_coords == 'undefined') {
		ymap = new ymaps.Map("ymap", {
	        center: [43.63, 51.16],
	        zoom: 12
	    });
	//Грузить карту с меткой, когда создают заявку    
	} else {
		ymap = new ymaps.Map("ymap", {
	        center: app_coords,
	        zoom: 12
	    });
	    
	    placemark = new ymaps.Placemark(app_coords);
		ymap.geoObjects.add(placemark);
	}
    
    ymap.events.add('click', function(e){
    	var coords = e.get('coords');
    	
    	if(placemark == undefined) {
			placemark = new ymaps.Placemark(coords);
			ymap.geoObjects.add(placemark);
		} else {
			placemark.geometry.setCoordinates(coords);
		}
		
		$("#application-coords").val(coords);
		$(".controller-coords").val(coords);
    		
    });
    
}

function init_view_map() {
	ymap = new ymaps.Map("ymap_view", {
        center: app_coords,
        zoom: 12
    });
    placemark = new ymaps.Placemark(app_coords);
	ymap.geoObjects.add(placemark);
}

$.fn.main_category_change = function () {
	var new_cat = $(this).val();
	$.ajax({
		url: "/ajax/application-subcategories",
		type: "POST",
		dataType: JSON,
		data: {
			id: new_cat
		},
		complete: function(data) {
			
			var result = $.parseJSON(data.responseText);
			var new_data = '<option value>Выберите подкатегорию </option>';;
			$.each(result, function(k, v) {
				new_data += '<option value='+k+'>'+v+'</option>';	
			});
			
			$('#application-application_type').html(new_data);
		},
		error: function(data) {
			$('#application-application_type').html("");
		},
		beforeSend: function() {
			$('#application-application_type').html("");
			$('#application-application_type').val("");
		}	
	});
	
}