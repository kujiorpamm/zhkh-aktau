<?php

namespace app\modules\application\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use kujiorpamm\cropit\widgets\CropitWidget;

use app\modules\application\models\ApplicationTypes;


class AdminController extends Controller
{
    public $layout = "@app/modules/workplace/views/layouts/main";
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ]
                    
                ],
            ]
        ];
    }
    
    public function actionApplicationTypes() {
		$model = new ApplicationTypes();
		
		$all = ApplicationTypes::find()->where(['=', 'parent_id', 0])->all();
		
		if($model->load(Yii::$app->request->post())) {
			
			if($model->icon) {
				$filename = "/images/icons/" . uniqid() . ".png";
				CropitWidget::saveAs($model->icon, $filename);
				$model->icon = $filename;
			}			
			
			$model->save();
			return $this->refresh();
		}
		
		return $this->render('application-types', ['model'=>$model, 'all'=>$all]);
	}
	
	public function actionEditApplicationType($id) {
		$model = ApplicationTypes::findOne(['id'=>$id]);
		$all = ApplicationTypes::find()->where(['=', 'parent_id', 0])->all();
		
		$old_icon = $model->icon;
		
		if($model->load(Yii::$app->request->post())) {
			
			if($_POST['ApplicationTypes']['icon']) {
				$filename = "images/icons/" . uniqid() . ".png";
				CropitWidget::saveAs($model->icon, $filename);
				$model->icon = "/".$filename;
			} else {							
				$model->icon = $old_icon;
			}
			$model->save();
			return $this->redirect("/application/admin/application-types");
		}
		
		return $this->render('application-types_edit', ['model'=>$model, 'all'=>$all]);
	}
	
	public function actionRemoveApplicationType($id) {
		$model = ApplicationTypes::findOne(['id'=>$id]);
		
		foreach($model->childs as $md) {
			$md->delete();
		}
		$model->delete();
		return $this->redirect(Yii::$app->request->referrer);
	}
    
}
