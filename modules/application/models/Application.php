<?php

namespace app\modules\application\models;

use Yii;
use app\modules\application\models\ApplicationTypes;
use app\modules\application\models\ApplicationFiles;
use app\modules\application\models\ApplicationAssignment;
use app\modules\application\models\ApplicationCommends;
use app\modules\organization\models\Organization;
use app\modules\user\models\User;
use yii\helpers\Url;

/**
 * This is the model class for table "application".
 *
 * @property integer $id
 * @property integer $application_type
 * @property integer $author
 * @property integer $active
 * @property string $description
 * @property string $date_created
 * @property string $date_plane_end
 * @property string $date_end
 * @property string $coords
 * @property integer $seriosness
 * @property integer $status
 * @property string $address
 * @property string $applicant_name
 * @property string $applicant_phone
 * @property string $applicant_addr
 */
class Application extends \yii\db\ActiveRecord
{

    public $image_load; // Переменная для загрузки фотографий
    public $main_category;
    public $statusName;
    public $date_created_human;
    public $date_end_human;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'application';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['application_type', 'author', 'description', 'date_created', 'date_plane_end', 'coords'], 'required'],
            [['application_type', 'author', 'active', 'seriosness', 'status'], 'integer'],
            [['date_created', 'date_plane_end', 'date_end', 'last_updated'], 'safe'],
            [['description'], 'string', 'max' => 5000],
            [['coords', 'applicant_name', 'applicant_phone'], 'string', 'max' => 500],
            [['address'], 'string', 'max' => 1000],
            [['applicant_addr'], 'string', 'max' => 1500],
            [['image_load', 'main_category', 'uncomplete_reason'], 'safe'],
            [['image_load'], 'required', 'on'=>'user-create']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'application_type' => Yii::t('app', 'Категория заявки'),
            'author' => Yii::t('app', 'Автор'),
            'authorName' => Yii::t('app', 'Автор'),
            'active' => Yii::t('app', 'Активна?'),
            'description' => Yii::t('app', 'Описание'),
            'date_created' => Yii::t('app', 'Дата создания'),
            'date_plane_end' => Yii::t('app', 'Дата планируемого завершения'),
            'date_end' => Yii::t('app', 'Дата завершения'),
            'coords' => Yii::t('app', 'Координаты'),
            'seriosness' => Yii::t('app', 'Важность'),
            'status' => Yii::t('app', 'Статус'),
            'address' => Yii::t('app', 'Адрес'),
            'applicant_name' => Yii::t('app', 'Заявитель - имя'),
            'applicant_phone' => Yii::t('app', 'Заявитель - телефон'),
            'applicant_addr' => Yii::t('app', 'Заявитель - адрес'),
            'fulltype' => Yii::t('app', 'Категория'),
            'statusName' => Yii::t('app', 'Статус'),
            'date_created_human' => Yii::t('app', 'Дата создания'),
            'organizationNames' => Yii::t('app', 'Исполнители'),
            'image_load' => Yii::t('app', 'Изображение'),
            'link' => Yii::t('app', 'Ссылка'),
            'uncomplete_reason' => Yii::t('app', 'Причина неисполнения'),
        ];
    }

    public function getImageBefore() {

		$model = ApplicationFiles::find()->where(['=', 'application_id', $this->id])->andWhere(['=', 'type', '1'])->one();
		if($model) return $model->src;
		if(!$model) return "/images/style/no-image.png";
	}

    public function getImageAfter() {

		$model = ApplicationFiles::find()->where(['=', 'application_id', $this->id])->andWhere(['=', 'type', '2'])->one();
		if($model) return $model->src;
		if(!$model) return "/images/style/no-image.png";
	}

	public function getFulltype() {
		return $this->type->name . " / " . $this->subtype->name;
	}

	public function getType() {
		return ApplicationTypes::findOne(['id'=>$this->subtype->parent_id]);
	}

	public function getAuthorModel() {
		return User::findOne(['id'=>$this->author]);
	}

	public function getAuthorName() {
		$model = User::findOne(['id'=>$this->author]);

		if($organization = $model->organization) {
			return $organization->name;
		}

		if(Yii::$app->user->can('admin') || Yii::$app->user->can('moderator')) return $model->fullname;
		return $model->hiddenName;
	}

	public function getSubtype() {
		return ApplicationTypes::findOne(['id'=>$this->application_type]);
	}

	public function getStatusName() {
		return $this->statusName;
	}

	public function getOrganizations() {
		return Organization::find()->innerJoin('application_assignment as', 'as.organization_id = organization.id')
									->innerJoin('application app', "app.id = as.application_id")->where("app.id = {$this->id}")->all();
	}

	public function getOrganizationNames() {
		$arr = [];
		foreach($this->organizations as $org) {
			 $arr[] = $org->name;
		}
		return implode($arr, ",");
	}

	public function getCommendsRaw() {
		foreach($this->getCommends() as $commend) {
			if($commend->author->isOrganizationUser()) $raw_commend .= $commend->author->organization->name . ":" . $commend->text ."\n";
		}
		return $raw_commend;
	}

	public function getLink() {
		return \yii\helpers\Html::a("<i class='glyphicon glyphicon-eye-open'/>", \yii\helpers\Url::to('/workplace/default/view?id='.$this->id, true) );
	}

	//remove
	public function getAssignments() {
		return ApplicationAssignment::find()->where(['=', 'application_id', $this->id])->all();
	}

	public function getAppAss_rel() {
		return $this->hasMany(ApplicationAssignment::className(), ['application_id'=>'id']);
	}

	//remove
	public function getCommends() {
		return ApplicationCommends::find()->where(['=', 'application_id', $this->id])->orderBy(['date'=>SORT_DESC])->all();
	}

	public function convertTime() {

	}


	public function update_address() {
		$this->address = self::decode_geo($this->coords);
		$this->save();
	}

	public static function decode_geo($lat_long) {
		$lat_long = explode(',', $lat_long);
		$sql = "SELECT id FROM gis_areas WHERE ST_CONTAINS(geometry, GeomFromText('POINT({$lat_long[1]} $lat_long[0])', 4294967294)) LIMIT 1";
		$result = Yii::$app->db->createCommand($sql)->queryAll();
		if($result) return $result[0]['id'];
		return "Не определено";
	}



	public function afterFind() {
		$this->statusName = Yii::$app->params['application']['status'][$this->status];

		$origin_date_created = $this->date_created;
		$origin_date_end = $this->date_end;
		$this->date_created = \Yii::$app->formatter->asDate($this->date_created, 'php:d.m.Y H:i:s');
		$this->date_plane_end = \Yii::$app->formatter->asDate($this->date_plane_end, 'php:d.m.Y H:i:s');
		$this->date_end = \Yii::$app->formatter->asDate($this->date_end, 'php:d.m.Y H:i:s');
		$this->date_created_human = $this->convert_to_human($origin_date_created);
		$this->date_end_human = $this->convert_to_human($origin_date_end);

	}

	public function beforeSave($insert) {
		$date = new \DateTime('now', new \DateTimeZone('Asia/Aqtau')); // Текущая дата
		$this->last_updated = $date->format('Y-m-d H:i:s');
		if (parent::beforeSave($insert)) {
			$this->address = self::decode_geo($this->coords);

			$origin_date_created = $this->date_created;
			$origin_date_end = $this->date_end;
			$this->date_created = \Yii::$app->formatter->asDate($this->date_created, 'php:Y-m-d H:i:s');
			$this->date_plane_end = \Yii::$app->formatter->asDate($this->date_plane_end, 'php:Y-m-d H:i:s');
			$this->date_end = \Yii::$app->formatter->asDate($this->date_end, 'php:Y-m-d H:i:s');

	        return true;
	    }
	    return false;
	}

	public function afterSave($insert, $changedAttributes){
	    parent::afterSave($insert, $changedAttributes);

	    /*После сохранения заявки обновить ApplicationAssignment - дату*/
	    foreach($this->assignments as $assignment) {
			$assignment->application_end = $this->date_plane_end;
			$assignment->save();
		}

		// Если это новая запись, отправить на почту уведомление
    if($insert && isset(Yii::$app->user->identity)) {
			Yii::$app->user->identity->message(
				Yii::t('app', 'Заявка №{1} создана на сайте {0}', [Url::to(["/"], true), $this->id]),
				Yii::t('app', "Посмотреть статус заявки можно по ссылке <a href='{0}'>{0}</a>", [Url::to(["/application/".$this->id], true)])
			);
		}

		// Если обновился статус заявки, отправить уведомление на почту
		if(!$insert && $changedAttributes['status']) {
			$this->getAuthorModel()->message(
				Yii::t('app', 'Заявка №{0}: статус обновлен', [$this->id]),
				Yii::t('app', "Посмотреть статус заявки можно по ссылке <a href='{0}'>{0}</a>", [Url::to(["/application/".$this->id], true)])
			);
		}
	}

	// Превратить в понятную дату
	protected function convert_to_human($origin) {

		if(!preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/', $origin) ) {
			 return $origin;
		}

		$date1 = new \DateTime('now', new \DateTimeZone('Asia/Aqtau')); // Текущая дата
		$date2 = new \DateTime($origin, new \DateTimeZone('Asia/Aqtau'));  // Дата из параметров
		$date3 = new \DateTime(date('Y-m-d H:i:s',strtotime("-1 days")), new \DateTimeZone('Asia/Aqtau'));  // Дата вчера

		$result = strtotime($date1->format('Y-m-d H:i:s')) - strtotime($date2->format('Y-m-d H:i:s')); // Разница в секундах

		$isCreatedToday = $date1->format('Y-m-d') == $date2->format('Y-m-d');
		$isCreatedYesterday = $date2->format('Y-m-d') == $date3->format('Y-m-d');

		if($isCreatedToday) { //Если сегодня

			if($result / 3600 < 1) { // Если не более часа
				$minutes = intval($result / 60);
				return Yii::t('app', '{0} мин. назад', [$minutes]);
			} else if ($result / 3600 > 1) {
				$hours = intval($result / 3600); // Если более часа
				switch ($hours) {
					case 1:
						return Yii::t('app', 'Час назад', [$hours]);
						break;
					case 2:
						return Yii::t('app', '{0} часа назад', [$hours]);
						break;
					case 3:
						return Yii::t('app', '{0} часа назад', [$hours]);
						break;
					case 4:
						return Yii::t('app', '{0} часа назад', [$hours]);
						break;
					case 21:
						return Yii::t('app', '{0} час назад', [$hours]);
						break;
					case 22:
						return Yii::t('app', '{0} часа назад', [$hours]);
						break;
					case 23:
						return Yii::t('app', '{0} часа назад', [$hours]);
						break;
					default:
						return Yii::t('app', '{0} часов назад', [$hours]);
						break;
				}
			}

		} else if($isCreatedYesterday) { // Если вчера
			return Yii::t('app', 'Вчера в {0}', [$date2->format('H:i')]);
		} else {
			return $date2->format('d.m.Y H:i');
		}
	}

	// Последние выполненные 3 заявки, где есть картинка. Нужна для главной страницы
	public static function getLastTheeCompletedWithImages( ) {
		return Application::find()->innerJoin(['application_files', 'application_files.application_id = application.id'])
			->where(['=', 'status', 3])->andWhere(['=', 'application_files.type', 2])
			->limit(3)->groupBy('application.id')->orderBy(['application.date_end' => SORT_DESC])->all();
	}

	public function isCompleted() {
		return $this->status == 3;
	}

	public function isProcessing() {
		return in_array($this->status, [1,2,0]);
	}

	public function isDeleted() {
		return $this->status == -1;
	}

}
