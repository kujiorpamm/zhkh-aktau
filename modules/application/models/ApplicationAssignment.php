<?php

namespace app\modules\application\models;

use Yii;
use app\modules\organization\models\Organization;
use app\modules\organization\models\OrganizationContract;
use yii\helpers\Url;

/**
 * This is the model class for table "application_assignment".
 *
 * @property integer $id
 * @property integer $application_id
 * @property integer $organization_id
 * @property integer $contract_id
 * @property string $application_end
 */
class ApplicationAssignment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'application_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['application_id', 'organization_id', 'application_end'], 'required'],
            [['application_id', 'organization_id'], 'unique', 'targetAttribute' =>['application_id', 'organization_id']],
            [['application_id', 'organization_id', 'contract_id'], 'integer'],
            [['application_end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'application_id' => Yii::t('app', 'Application ID'),
            'organization_id' => Yii::t('app', 'Organization ID'),
            'contract_id' => Yii::t('app', 'Contract ID'),
            'application_end' => Yii::t('app', 'Application End'),
        ];
    }
    
    public function getOrganization() {
		return Organization::findOne(['id'=>$this->organization_id]);
	}
    
    public function getContract() {
		return OrganizationContract::findOne(['id'=>$this->contract_id]);
	}
	
	public function processAssignment($request) {
		if($this->load($request) && $this->save()) {
			foreach($this->getOrganization()->getUsers() as $user){
				$user->message(
					"Вам адресована новая заявка № ".$this->application_id." на сайте " . Url::to(["/"], true),
					"Информацию о заявке можно посмотреть на рабочем столе портала " . "<a href='".Url::to(["/workplace"], true)."'>".Url::to(["/workplace"], true)."</a>"
				);
			}
			return true;
		}
	}
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {	        
	        
	        if(preg_match('/\d{2}\.\d{2}\.\d{4}/', str_replace(' ', '', $this->application_end) )) {
	        	$newdate = \DateTime::createFromFormat('d.m.Y', str_replace(' ', '', $this->application_end) );
	        	$this->application_end = \Yii::$app->formatter->asDatetime($newdate, "php:Y-m-d");
	        }
	        
	        return true;
	    }
	    return false;
	}
}
