<?php

namespace app\modules\application\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "application_commends".
 *
 * @property integer $id
 * @property integer $application_id
 * @property string $date
 * @property integer $author_id
 * @property string $text
 */
class ApplicationCommends extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'application_commends';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['application_id', 'date', 'author_id', 'text'], 'required'],
            [['application_id', 'author_id'], 'integer'],
            [['date'], 'safe'],
            [['text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'application_id' => Yii::t('app', 'Application ID'),
            'date' => Yii::t('app', 'Date'),
            'author_id' => Yii::t('app', 'Author ID'),
            'text' => Yii::t('app', 'Text'),
        ];
    }

    public function getApplication() {
		return Application::findOne(['id'=>$this->application_id]);
	}

	public function getAuthor() {
		return User::findOne(['id'=>$this->author_id]);
	}



	public function beforeValidate() {
	    if (parent::beforeValidate()) {

	        if(!$this->author_id) $this->author_id = Yii::$app->user->id;
          if(!$this->date) $this->date = date("Y-m-d H:i:s");


	        return true;
	    }
	    return false;
	}

	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {

	        //Заменить в тексте IMG, дать им класс
	        $this->text = str_replace("<img", "<img class='img img-in-text img-popup'", $this->text);
	        return true;
	    }
	    return false;
	}
}
