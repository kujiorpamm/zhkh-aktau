<?php

namespace app\modules\application\models;

use Yii;

 
class ApplicationSearch extends Application
{
    
    public $date1;
    public $date2;
    public $types;
    public $dists;
    
    function __construct() {    	
        $this->types = \app\modules\application\models\ApplicationTypes::getAllAsArray();
    	$this->dists = 	(new \yii\db\Query())->select('name')->from('gis_areas')->indexBy('id')->orderBy('name')->column();
    }
    
    public function rules()
    {
        return [
            [['date1', 'date2'], 'safe'],
        ];
    }
    
}
