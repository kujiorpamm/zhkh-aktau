<?php

namespace app\modules\application\models;

use app\modules\application\models\Application;

use Yii;

/**
 * This is the model class for table "application_types".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name_ru
 * @property string $name_kz
 * @property string $icon
 */
class ApplicationTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'application_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'name_ru'], 'required'],
            [['parent_id'], 'integer'],
            [['name_ru', 'name_kz'], 'string', 'max' => 300],
            [['icon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Родительская категория'),
            'name_ru' => Yii::t('app', 'Наименование (рус)'),
            'name_kz' => Yii::t('app', 'Наименование (каз)'),
            'icon' => Yii::t('app', 'Иконка для карты'),
        ];
    }
    
    /*public function getMainTypesAsArray() {
		return $this::find()->select('name_ru')->indexBy('id')->where(['=', 'parent_id', 0])->column();
	}*/
	
	public static function getMainTypesAsArray() {
		return ApplicationTypes::find()->select('name_ru')->indexBy('id')->where(['=', 'parent_id', 0])->column();
	}
	
	public static function getSubTypesAsArray($id) {
		return ApplicationTypes::find()->select('name_ru')->indexBy('id')->where(['=', 'parent_id', $id])->column();
	}
	
	public static function getAllAsArray() {
		$head = self::getMainTypesAsArray();
		foreach($head as $key=>$val) {
			$new[$val] = self::getSubTypesAsArray($key);
			/*$head[$key] = [
				'name' => $val,
				'items' => self::getSubTypesAsArray($key)
			];*/
		}
		return $new;
	}
	
	public function getChilds() {
		return $this->find()->where(['=', 'parent_id', $this->id])->all();
	}
	
	public function getParent() {
		return self::findOne(['id'=>$this->parent_id]);
	}
	
	public function getApplications($arr = false, $short_info = false) {
		$q = Application::find();
		if($short_info) $q->select(['id', 'coords', 'date_created']);
		if($arr) $q->asArray();
		//echo "<pre>"; print_r($this->id); echo "</pre>";
		return $q->where(['=', 'application_type', $this->id])->all();
		
	}
	
	public function getName() {
		switch(Yii::$app->language){
			case 'ru':
				return $this->name_ru;
				break;
			case 'kk':
				return $this->name_kz;
				break;
			
			default:
			return $this->name_ru;
				break;
		}
	}
	
	public static function generateForMenu() {
		$categores = self::find()->where(['parent_id'=>0])->all();
		$categories_array = [];
		foreach($categores as $category) {
			
			$subcats = self::find()->where(['parent_id'=>$category->id])->all();
			foreach($subcats as $cs) {
				$_subcats[] = [
					'id' => $cs->id,
					'name' => $cs->name,
					'icon' => $cs->icon,
					'apps' => $cs->getApplications(true, true)
				];
			}
						
			$categories_array[] = [
				'id' => $category->id,
				'name' => $category->name,
				'icon' => $category->icon,
				'items' => $_subcats
			];
			$_subcats = [];
		}
		
		//echo "<pre>"; print_r($categories_array); echo "</pre>"; exit;
		
		return $categories_array;
	}
	
	public function afterFind() {
		
		// Если нет иконки, указать родительскую 
		if($this->parent_id !== 0 && !$this->icon) {
			$this->icon = $this->parent->icon;
		}
		
	}
	
	
}
