<?php

namespace app\modules\application;

/**
 * application module definition class
 */
class module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\application\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

         $this->setAliases([ 
            '@application' => __DIR__ . '/assets' 
        ]);
    }
}
