<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
?>

<?php	
	$types = [0=>'Родительская (содержит остальные)'];
	$types += $model->getMainTypesAsArray();
	//echo $model->icon; exit;
	$this->registerJs("$(document).ready(function(e) { $('.cropit-preview-image').attr('src', '/".$model->icon."') })");
	
?>

<div class="workplace">
	
	<div class="heading">
		<h3><?=Yii::t('app', 'Редактирование типа')?></h3>
		
	</div>
	
	<div class="application-container">
		<?php $form = ActiveForm::begin(); ?>
		<?= $form->errorSummary($model); ?>
			
			<?=$form->field($model, 'parent_id')->dropDownList($types, ['prompt'=>'Выберите категорию заявки'])->label(false)?>
			<?=$form->field($model, 'name_ru')->textInput(['placeholder'=>$model->getAttributeLabel('name_ru')])->label(false)?>
			<?=$form->field($model, 'name_kz')->textInput(['placeholder'=>$model->getAttributeLabel('name_kz')])->label(false)?>
			
			<div id="cropit_icon">
				<?= $form->field($model, 'icon')->widget(\kujiorpamm\cropit\widgets\CropitWidget::className(), [
					'pluginOptions' => [
						'width' => 30,
						'height' => 30,
						'smallImage' => 'allow'
				]]);?>
			</div>
			
			
			<button type="submit" class="btn btn-success btn-block"> <?=Yii::t('app', 'Обновить')?> </button>
			
		<?php ActiveForm::end(); ?>
	</div>
	
	
</div>
	

