<?php
	switch($model->status){
		case 0:
			$class = "status-0";
			break;
		case 1:
			$class = "status-1";
			break;
		case 2:
			$class = "status-2";
			break;
		case 3:
			$class = "status-3";
			break;
		
		default:
			break;
	}
?>

<div class="application-item <?=$class?>">	
		<div class="image" style="background-image: url('<?=$model->imageBefore?>')">
			
		</div>
		
		<div class="description">
			<b>№<?=$model->id?></b>  от <?=Yii::$app->formatter->asDate($model->date_created, 'php:d.m.Y')?>
			<div class="date-counter"><?=$model->date_created?></div> <hr/>
			<div><?=$model->fulltype?></div>
			<div><?=$model->description?></div>
		</div>
		<a href="/workplace/default/view?id=<?=$model->id?>"><div class="overlap"></div></a>
	
	
</div>