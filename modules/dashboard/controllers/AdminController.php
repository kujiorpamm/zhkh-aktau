<?php

namespace app\modules\dashboard\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use app\modules\dashboard\models\Dashboard;

/**
 * Default controller for the `dashboard` module
 */
class AdminController extends Controller
{
    
    public $layout = "@app/modules/workplace/views/layouts/main";
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'moderator', 'maek'],
                    ]
                    
                ],
            ]
        ];
    }
    
    public function actionStats()
    {
        $model = new Dashboard;
        $model->author_id = Yii::$app->user->id;
        
        if($model->load(Yii::$app->request->post())) {
			$model->date = Yii::$app->formatter->asDate($model->date, 'php:Y-m-d');
			if($model->save()) {
				return $this->refresh();
			} else {				
				Yii::$app->session->setFlash('error', "1");	
			}			
		}
        
        $data_provider = new ActiveDataProvider([
            'query' => Dashboard::find(),
            'sort' => [
				'defaultOrder' => [
					'date' => SORT_DESC,
					'date_human' => SORT_DESC
				],
				'attributes' => [
					'date_human' => [
						'desc' => ['date'=>SORT_DESC],
						'asc' => ['date'=>SORT_ASC],
					]
				]
			]
        ]);
        
        return $this->render('index', ['dp'=>$data_provider, 'model'=>$model]);
    }
    
    public function actionDelete($id) {
		Dashboard::findOne(['id'=>$id])->delete();
		return $this->redirect(Yii::$app->request->referrer);
	}
    
    
    public function actionLoadFile(){
		$blob = Yii::$app->request->post('blob');
		if(!$blob) return;
		
		$file_content = str_replace("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,", "", $blob);
		$file_content = base64_decode($file_content);
		$path = "tmp/testfile.xlsx";
		$file = fopen($path, "w");
		fwrite($file, $file_content);
		
		$ppp = new \moonland\phpexcel\Excel();
		
		$data = \moonland\phpexcel\Excel::import($path, ['setFirstRecordAsKeys'=>false]);
		$sheet = $data[0];
		
		$model = new Dashboard();
		$model->date = "123";
		foreach($sheet as $row) {			
			$model[$row['C']] = $row['B'];
		}
		
		
		echo \yii\helpers\Json::encode($model) ;
		
		
	}
    
}
