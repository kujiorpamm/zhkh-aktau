<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Показания МАЭК');

$this->registerJsFile('/js/maek.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<div class="workplace">
	
	<div class="heading">
		<h3><?= Html::encode($this->title) ?></h3>
		
	</div>
	
	<div class="application-container">
				
		<div class="stat-form">
			
			<!--EXCEL-->
			<div class="row excel-row">
				<div class="col-sm-3">
					
				</div>
				<div class="col-sm-4">
					<input id="ex_file" type="file" class="form-control" name="file"/>
				</div>
				<div class="col-sm-1">
					<div class="loader sm" style="display: none;"></div>
				</div>
				<div class="col-sm-4 btns">
					<button id="ex_btn" class="btn btn-success btn-sm btn-block">Загрузить Excel</button> <br />
					<a href="/vendor/dashboard/template.xlsx" target="_blank"><?=Yii::t('app', 'Скачать файл-шаблон')?></a>
				</div>
			</div>
			<!--КОНЕЦ EXCEL-->
			
			<!--ФОРМА-->
			<div class="form form-row" style="display: none;">						
				<?php $form = ActiveForm::begin([
					'fieldConfig' => [
						'template' => '<div class="cc col-sm-3">{label}{input}{hint}</div>'
					]			
					
				]); ?>
				
				<div class="row">
					
					
					<div class="cc col-sm-3">
						<?=$form->field($model, 'date', [ 'template' => '<div>{label}{input}{hint}</div>'])->widget(DatePicker::classname(), [
						    'options' => ['placeholder' => Yii::t('app', 'Укажите дату')],
						    'removeButton' => false,
						    'pluginOptions' => [
						        'format' => 'dd.mm.yyyy',
						        'autoclose' => true,
						    ]
						]);?>
					</div>
					
					<?php
						foreach($model->attributes() as $attr) {
							if(!in_array($attr, ['id', 'author_id', 'date'])) echo $form->field($model, $attr);
						}
					?>
					
				
					
				</div>
				<div class="row agree-row">
					<div class="col-sm-6 col-sm-offset-3">
						<?=$form->field($model, 'agree')->checkbox()->label(Yii::t('app', 'Я подтверждаю, что данные введены верно'));?>
					</div>
				</div>
				<div class="row submit-row">
					<div class="col-sm-6 col-sm-offset-3">
						<button type="submit" class="btn btn-success btn-block"><?=Yii::t('app', 'Добавить')?></button>
					</div>
				</div>
				
				
				<?php
					if(Yii::$app->session->hasFlash('error')) {
						$this->registerJs("error_modal('".$form->errorSummary($model)."')");
					}
				?>
				
				<?php ActiveForm::end(); ?>
			</div>
			<!--КОНЕЦ ФОРМА-->
			
		</div>
		
		<div class="list">
		
		    <?= GridView::widget([
		        'dataProvider' => $dp,
		        'columns' => [
		        	'id', 
		        	'date_human',
		        	[
		        		'format' => 'raw',
		        		'attribute' => 'removeLink',
		        		'label' => false
		        	]
		        ]
		    ]); ?>
	    
	    </div>
	</div>
	
	
</div>

