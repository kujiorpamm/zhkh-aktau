<?php

namespace app\modules\news\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $author_id
 * @property integer $organization_id
 * @property string $date
 * @property string $title_ru
 * @property string $title_kz
 * @property string $text_ru
 * @property string $text_kz
 */
class News extends \yii\db\ActiveRecord
{
    
    public $date_human;
    public $date_month_year;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'title_ru', 'text_ru'], 'required'],
            [['category_id', 'organization_id'], 'integer'],
            [['date'], 'safe'],
            [['text_ru', 'text_kz'], 'string'],
            [['author_id'], 'string', 'max' => 6],
            [['title_ru', 'title_kz'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Категория'),
            'author_id' => Yii::t('app', 'Автор'),
            'organization_id' => Yii::t('app', 'Организация'),
            'date' => Yii::t('app', 'Дата'),
            'date_human' => Yii::t('app', 'Дата'),
            'title_ru' => Yii::t('app', 'Заголовок (рус)'),
            'title_kz' => Yii::t('app', 'Заголовок (каз)'),
            'title' => Yii::t('app', 'Заголовок'),
            'text_ru' => Yii::t('app', 'Текст (рус)'),
            'text_kz' => Yii::t('app', 'Текст (каз)'),
            'link_view' => '',
            'link_delete' => '',
            'link_edit' => '',
        ];
    }
    
    public function getTitle() {
		return $this->title_ru;
	}
    
    public function getText() {
		return $this->text_ru;
	}
	
	public function getLink_view() {
		return "<a href='/news/{$this->id}'><i class='glyphicon glyphicon-eye-open'></i></a>";
	}
	
	public function getLink_edit() {
		return "<a href='/news/admin/edit-news?id={$this->id}'><i class='glyphicon glyphicon-edit'></i></a>";
	}
	
	public function getLink_delete() {
		return "<a class='confirm-link' href='/news/admin/delete-news?id={$this->id}'><i class='glyphicon glyphicon-remove'></i></a>";
	}
    
    public function afterFind() {
		$this->date = Yii::$app->formatter->asDate($this->date, 'php:d.m.Y H:i:s');
		$this->date_human = $this->convert_to_human($this->date);
		$this->date_month_year = Yii::t('app', Yii::$app->formatter->asDate($this->date, 'php:F')) . Yii::$app->formatter->asDate($this->date, 'php:, Y');		
	}
    
    // Превратить в понятную дату
	protected function convert_to_human($origin) {
		
		$date1 = new \DateTime('now', new \DateTimeZone('Asia/Aqtau')); // Текущая дата
		$date2 = new \DateTime($origin, new \DateTimeZone('Asia/Aqtau'));  // Дата из параметров
		$date3 = new \DateTime(date('Y-m-d H:i:s',strtotime("-1 days")), new \DateTimeZone('Asia/Aqtau'));  // Дата вчера		
		
		$result = strtotime($date1->format('Y-m-d H:i:s')) - strtotime($date2->format('Y-m-d H:i:s')); // Разница в секундах		
		
		$isCreatedToday = $date1->format('Y-m-d') == $date2->format('Y-m-d');		
		$isCreatedYesterday = $date2->format('Y-m-d') == $date3->format('Y-m-d');		
		
		if($isCreatedToday) { //Если сегодня
			
			if($result / 3600 < 1) { // Если не более часа
				$minutes = intval($result / 60);
				return Yii::t('app', '{0} мин. назад', [$minutes]);
			} else if ($result / 3600 > 1) {
				$hours = intval($result / 3600); // Если более часа
				switch ($hours) {
					case 1:
						return Yii::t('app', 'Час назад', [$hours]);
						break;
					case 2:
						return Yii::t('app', '{0} часа назад', [$hours]);
						break;
					case 3:
						return Yii::t('app', '{0} часа назад', [$hours]);
						break;
					case 4:
						return Yii::t('app', '{0} часа назад', [$hours]);
						break;
					case 21:
						return Yii::t('app', '{0} час назад', [$hours]);
						break;
					case 22:
						return Yii::t('app', '{0} часа назад', [$hours]);
						break;
					case 23:
						return Yii::t('app', '{0} часа назад', [$hours]);
						break;
					default:
						return Yii::t('app', '{0} часов назад', [$hours]);
						break;
				}								
			}
			
		} else if($isCreatedYesterday) { // Если вчера
			return Yii::t('app', 'Вчера в {0}', [$date2->format('H:i')]);
		} else {
			return $date2->format('d.m.Y H:i');
		}
	}
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
	        
	        /*Если новая запись, то добавить автора и организацию*/
	        if($insert){
				$this->author_id = Yii::$app->user->id;
	        	$this->organization_id = Yii::$app->user->identity->organization->id;
			}
	        
	        
	        
	        if(preg_match('/\d{2}\.\d{2}\.\d{4} \d{2}:\d{2}:\d{2}/', $this->date )) {
	        	$newdate = \DateTime::createFromFormat('d.m.Y H:i:s', $this->date );
	        	$this->date = \Yii::$app->formatter->asDatetime($newdate, "php:Y-m-d H:i:s");
	        }
	        
	        return true;
	    }
	    return false;
	}
	
}
