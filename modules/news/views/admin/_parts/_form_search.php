<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use kartik\export\ExportMenu;
?>

<?php ActiveForm::begin(); ?>
<div class="row search">
	<div class="col-sm-4">
		<div class="input-group">
			<?=Html::textInput('search-word', $search, ['class'=>'form-control', 'placeholder'=>Yii::t('app', 'Поиск')])?>
			<span class="input-group-btn">
		        <button class="btn btn-secondary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
		      </span>
		</div>
	</div>
	<div class="col-sm-5 search-text">
		<?php if($search): ?>
		<a href="/news/admin"><?=Yii::t('app', 'Сбросить результаты поиска')?></a>
		<?php endif; ?>
	</div>
	<div class="col-sm-3 search-text">
		<div class="clearfix">
			<div class="pull-right">				
				<button class="btn btn-success" onclick="$('#m_form').modal('show');"><?=Yii::t('app', 'Добавить новость')?></button>
			</div>
		</div>
	</div>
	
</div>	
<?php ActiveForm::end(); ?>

<div class='clearfix'>
	<div class='pull-left'>{summary}</div>
	<div class='pull-right'>{pager}</div>
</div>

