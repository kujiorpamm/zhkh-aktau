<?php

namespace app\modules\notification\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\application\models\Application;

/**
 * Default controller for the `notification` module
 */
class DefaultController extends Controller
{
    
    public $layout = "@app/modules/workplace/views/layouts/main";
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ]
                    
                ],
            ]
        ];
    }
    
    public function actionIndex()
    {
        $month_ago_date = date('Y-m-d', strtotime("-30 days"));
        $uncompleted_applications = Application::find()
        		->where(['IN', 'status', [0, 1, 2]])
        		->andWhere(['<=', 'DATE(date_created)', $month_ago_date])
        		->andWhere(['IS', 'uncomplete_reason', NULL])
        		->all();
        
        return $this->render('index', [
        	'notifications' => [
        		'uncompleted_need_reason' => $uncompleted_applications
        	]
        ]);
    }
}
