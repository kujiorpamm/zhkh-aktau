<?php

namespace app\modules\organization\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\modules\organization\models\Organization;
use app\modules\organization\models\OrganizationProjects;
use app\modules\organization\models\OrganizationReports;
use app\modules\application\models\Application;
use app\modules\application\models\ApplicationAssignment;

/**
 * Default controller for the `organization` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $models = Organization::find()->where(['!=', 'id', 4])->orderBy(['name'=>SORT_ASC])->all();

        return $this->render('index', ['models'=>$models]);
    }

    public function actionView($id) {
		$model = Organization::findOne(['id'=>$id]);

		$app_q = Application::find()->leftJoin(ApplicationAssignment::tableName(), 'application.id = application_assignment.application_id')->where("organization_id = {$model->id}");
		$proj_q = OrganizationProjects::find()->where("organization_id = {$model->id}")->andWhere("date_2 > NOW()");

		$dp = new ActiveDataProvider([
			'query' => $app_q,
			'pagination' => [
				'pageSize' => 5,
			],
			'sort' => [
				'defaultOrder' => [
					'date_end' => SORT_DESC
				]
			]
		]);
		$dp2 = new ActiveDataProvider([
			'query' => $proj_q,
			'pagination' => [
				'pageSize' => 5,
			],
			'sort' => [
				'defaultOrder' => [
					'date_1' => SORT_DESC
				]
			]
		]);

    $organization_reports = new OrganizationReports();
    $organization_reports->organization_id = $model->id;

		return $this->render('view', ['model'=>$model, 'dp'=>$dp, 'dp2'=>$dp2, 'dp_reports'=>$organization_reports->search() ]);
	}

  public function actionViewReport($id) {
    $model = OrganizationReports::findOne(['id'=>$id]);
    if(!$model) throw new \Exception(Yii::t('app', 'Страница не найдена'), 1);
    return $this->render('view-report', ['model'=>$model]);

  }

}
