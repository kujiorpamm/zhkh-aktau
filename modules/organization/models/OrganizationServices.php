<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "organization_services".
 *
 * @property integer $id
 * @property integer $organization_id
 * @property string $name_ru
 * @property string $name_kz
 * @property string $description_ru
 * @property string $description_kz
 * @property integer $price
 */
class OrganizationServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_id', 'name_ru', 'description_ru', 'price'], 'required'],
            [['organization_id', 'price'], 'integer'],
            [['description_ru', 'description_kz'], 'string'],
            [['name_ru', 'name_kz'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'organization_id' => Yii::t('app', 'Organization ID'),
            'name_ru' => Yii::t('app', 'Наименование (рус)'),
            'name_kz' => Yii::t('app', 'Наименование (каз)'),
            'description_ru' => Yii::t('app', 'Описание (рус)'),
            'description_kz' => Yii::t('app', 'Описание (каз)'),
            'price' => Yii::t('app', 'Стоимость услуги'),
        ];
    }
    
    public function getName() {
		return $this->name_ru;
	}
    
}
