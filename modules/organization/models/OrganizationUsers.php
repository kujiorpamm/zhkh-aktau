<?php

namespace app\modules\organization\models;

use Yii;

use app\modules\user\models\User;
use app\modules\organization\models\Organization;

/**
 * This is the model class for table "organization_users".
 *
 * @property integer $id
 * @property integer $organization_id
 * @property integer $user_id
 * @property string $commend
 */
class OrganizationUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_id', 'user_id'], 'required'],
            [['organization_id', 'user_id'], 'unique', 'targetAttribute'=>['organization_id', 'user_id']],
            [['organization_id', 'user_id'], 'integer'],
            [['commend'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'organization_id' => Yii::t('app', 'Organization ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'commend' => Yii::t('app', 'Комментарий'),
        ];
    }
    
    public function getUser() {
		return $this->hasOne(User::className(), ['id'=>'user_id']);
	}
    
    public function getOrganization() {
		return $this->hasOne(Organization::className(), ['id'=>'organization_id']);
	}
    
    
}
