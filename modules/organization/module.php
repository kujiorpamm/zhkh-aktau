<?php

namespace app\modules\organization;

/**
 * organization module definition class
 */
class module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\organization\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->setAliases([ 
            '@organization' => __DIR__ . '/assets' 
        ]);
    }
}
