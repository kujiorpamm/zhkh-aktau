<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Modal;
	use yii\bootstrap\ActiveForm;
	use kartik\date\DatePicker;
?>
	
	
<?php	
	Modal::begin([
		'id' => 'm_form',
	    'header' => Yii::t('app', 'Добавление услуги в организацию'),
	    'size' => 'modal-md',
	    'options' => [
	    	'tabindex' => false
	    ]/*,
	    'clientOptions'=> [
	    	'show' => true
	    ]*/
	]);
?>
	
	<?php $form = ActiveForm::begin(); ?>
	<?= $form->errorSummary($model); ?>
		
		<?=$form->field($model, 'organization_id')->hiddenInput()->label(false)?>
		<?=$form->field($model, 'name_ru')->textInput(['placeholder'=>$model->getAttributeLabel('name_ru')])->label(false)?>
		<?=$form->field($model, 'name_kz')->textInput(['placeholder'=>$model->getAttributeLabel('name_kz')])->label(false)?>
		<?=$form->field($model, 'price')->textInput(['placeholder'=>$model->getAttributeLabel('price'), 'type'=>'number'])->label(false)?>
		
		<?= $form->field($model, 'description_ru')->widget(\yii\redactor\widgets\Redactor::className(), [
			'clientOptions' => [
				'minHeight' => 500,
				'lang' => 'ru',
				'minHeight' => '300px'
			]
		])?>
		<?= $form->field($model, 'description_kz')->widget(\yii\redactor\widgets\Redactor::className(), [
			'clientOptions' => [
				'minHeight' => 500,
				'lang' => 'ru',
				'minHeight' => '300px'
			]
		])?>
		
		<button type="submit" class="btn btn-success btn-block"> <?=Yii::t('app', 'Добавить')?> </button>
		
	<?php ActiveForm::end(); ?>
	
	
<?php
	Modal::end();
?>
