<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
?>

<div class="workplace">
	
	<div class="heading">
		<h3><?=$model->name?></h3>
		
	</div>
	
	<div class="application-container">
		<?php $form = ActiveForm::begin(); ?>
		<?= $form->errorSummary($model); ?>
			<?=$form->field($model, 'name_ru')->textInput(['placeholder'=>$model->getAttributeLabel('name_ru')])->label(false)?>
			<?=$form->field($model, 'name_kz')->textInput(['placeholder'=>$model->getAttributeLabel('name_kz')])->label(false)?>
			<?=$form->field($model, 'price')->textInput(['placeholder'=>$model->getAttributeLabel('price'), 'type'=>'number'])->label(false)?>
			
			<?= $form->field($model, 'description_ru')->widget(\yii\redactor\widgets\Redactor::className(), [
				'clientOptions' => [
					'minHeight' => 500,
					'lang' => 'ru',
					'minHeight' => '300px'
				]
			])?>
			<?= $form->field($model, 'description_kz')->widget(\yii\redactor\widgets\Redactor::className(), [
				'clientOptions' => [
					'minHeight' => 500,
					'lang' => 'ru',
					'minHeight' => '300px'
				]
			])?>
			
			<button type="submit" class="btn btn-success btn-block"> <?=Yii::t('app', 'Обновить')?> </button>
			
		<?php ActiveForm::end(); ?>
	</div>
	
	
</div>
	

	
