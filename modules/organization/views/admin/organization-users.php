<?php

use app\modules\organization\assets\OrganizationAsset;

OrganizationAsset::register($this);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Организации'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view-organization', 'id'=>$model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Список пользователей');
?>

<div class="workplace">
	
	<div class="heading">
		<div class="clearfix">
			<div class="pull-left">
				<h3><?=Yii::t('app', $model->name)?></h3>
				<?=$this->render('_parts/_organization_submenu', ['model'=>$model])?>
			</div>
			<div class="pull-right">
				<button class="btn btn-success" onclick="js:$('#m_form').modal('show');">
					<?=Yii::t('app', 'Добавить пользователя в организацию')?>
				</button>
			</div>
		</div>
			
		
	</div>
	
	<div class="application-container">
		<table class="table">
			<thead>
				<tr>
					<th><?=Yii::t('app', 'E-mail')?></th>
					<th><?=Yii::t('app', 'Имя')?></th>
					<th><?=Yii::t('app', '')?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
					
					foreach($model->users as $user) {
						echo "
							<tr>
								<td>{$user->email}</td>
								<td>
									<a href='/user/admin/view-user?id={$user->id}'>{$user->fname} {$user->lname}</a>
								</td>
								<td>
									<a href='/organization/admin/unlink-user?id={$user->id}' class='confirm-link'>
										<span class='glyphicon glyphicon-remove'></span>
									</a>
								</td>
							</tr>
						";
					}
					
				?>
			</tbody>
		</table>
	</div>
	
	
</div>



<?=$this->render('_parts/_organization_user_add_modal', ['model'=>$model_user, 'model_org'=>$model, 'users'=>$users])?>