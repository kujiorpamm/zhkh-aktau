<div class="project-item" data-attr="<?=$model->id?>">
	<div class="heading">
		<span class="icon"></span>
		<span class="date"> <span class="moment-date-base"><?=$model->date_1?></span> - <span class="moment-date-base"><?=$model->date_2?></span> </span>
		<div class="text"> <a href="/organizations/<?=$model->organization->id?>"><?=$model->organization->name?> </a> : <?=$model->name?></div>
	</div>
	<div class="content"><?=$model->descr?></div>
</div>