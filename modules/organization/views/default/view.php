<?php
	use yii\widgets\ListView;
?>

<div class="page-index" id="organization-view">

	<div class="wrap white">
		<div class="container">
			<h2><?=$model->name?></h2>

			<div class="row">
				<div class="col-sm-6">
					<div class="listing-container">
						<div class="item" style="display: <?=$model->contact?'':'none'?>">
							<div class="title"><?=Yii::t('app', 'Контактная информация')?></div>
							<div class="content"><?=$model->contact?></div>
						</div>
						<div class="item" style="display: <?=$model->activity?'':'none'?>">
							<div class="title"><?=Yii::t('app', 'Деятельность')?></div>
							<div class="content"><?=$model->activity?></div>
						</div>
						<div class="item" style="display: <?=$model->heading?'':'none'?>">
							<div class="title"><?=Yii::t('app', 'Правление')?></div>
							<div class="content"><?=$model->heading?></div>
						</div>
						<div class="item" style="display: <?=$model->rule?'':'none'?>">
							<div class="title"><?=Yii::t('app', 'Устав')?></div>
							<div class="content"><?=$model->rule?></div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 resources">

					<ul class="nav nav-tabs">
					  <li class="active"><a data-toggle="tab" href="#current_apps"><?=Yii::t('app', 'Текущие заявки')?></a></li>
					  <li><a data-toggle="tab" href="#planning_works"><?=Yii::t('app', 'Планируемые работы')?></a></li>
					  <li><a data-toggle="tab" href="#reports"><?=Yii::t('app', 'Отчеты')?></a></li>
					</ul>

					<div class="tab-content">
					  <div id="current_apps" class="tab-pane fade in active">
							<?php
								echo ListView::widget([
										 'dataProvider' => $dp,
										 'itemOptions' => ['class' => 'item'],
										 'itemView' => '_view_app_part',
										 'pager' => [
												'class' => \kop\y2sp\ScrollPager::className(),

												'triggerText' => Yii::t('app', 'Загрузить еще')
											]
								]);
							?>
					  </div>
					  <div id="planning_works" class="tab-pane fade">
							<?php
								echo ListView::widget([
										 'dataProvider' => $dp2,
										 'itemOptions' => ['class' => 'item'],
										 'itemView' => '_view_proj_part'
								]);
							?>
					  </div>
					  <div id="reports" class="tab-pane fade">
							<?php
								echo ListView::widget([
										 'dataProvider' => $dp_reports,
										 'itemOptions' => ['class' => 'item'],
										 'itemView' => function($model) {
											 	return "<div class='item'>
													<span class='date'>$model->date</span>
													<span class='title'>
														<a href='/organizations/reports/{$model->id}'>{$model->title}</a>
													</span>
												</div>";
										 }
								]);
							?>
					  </div>
					</div>

				</div>
			</div>



		</div>
	</div>

</div>
