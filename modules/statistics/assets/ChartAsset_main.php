<?php

namespace app\modules\statistics\assets;

use yii\web\AssetBundle;

class ChartAsset_main extends AssetBundle {
	
	public $sourcePath = '@app/modules/statistics/assets';
	public $publishOptions = [
	    'forceCopy' => true,
	];
	public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

	public $js = [
	    'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js',
	    'https://hammerjs.github.io/dist/hammer.min.js',
	    'https://unpkg.com/leaflet@1.2.0/dist/leaflet.js',
	    'js/chartjs-plugin-zoom.min.js',
	    'js/Charts.js',
	];
	
	public $css = [
		'https://unpkg.com/leaflet@1.2.0/dist/leaflet.css',
	    'css/dashboard.less',
	];
		
		
    public $depends = [
     	'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'app\assets\TimeAsset'
    ];
	
}