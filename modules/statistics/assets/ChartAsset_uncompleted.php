<?php

namespace app\modules\statistics\assets;

use yii\web\AssetBundle;

class ChartAsset_uncompleted extends AssetBundle {
	
	public $sourcePath = '@app/modules/statistics/assets';
	public $publishOptions = [
	    'forceCopy' => true,
	];
	public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

	public $js = [
	    'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js',
	    'js/Charts_uncompleted.js',
	];
	
	public $css = [
		'https://unpkg.com/leaflet@1.2.0/dist/leaflet.css',
	    'css/dashboard.less',
	];
		
		
    public $depends = [
     	'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'app\assets\TimeAsset'
    ];
	
}