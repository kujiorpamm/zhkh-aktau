var chart1,chart2, chart3, chart4, mymap, is_mobile, prev_is_mobile; 


$(window).resize(function(e){
	prev_is_mobile = is_mobile;
	is_mobile = $(window).width() <= 768;
	if(prev_is_mobile != is_mobile) {
		ajaxChart2();
		ajaxChart4();
	}
});

$(document).ready(function(){
	
	is_mobile = $(window).width() <= 768;
	
	moment.locale('ru');
	ajaxGeneral();
	ajaxChart1();
	ajaxChart2();	
	ajaxChart3();
	ajaxChart4();
	ajaxListView();
	
	
	ajaxMap();
	
	$("#filter").click(function(){
		ajaxGeneral();
		ajaxChart1();
		ajaxChart2();	
		ajaxChart3();
		ajaxChart4();
		ajaxMap();
		ajaxListView();
	});
	
	$('#pdf').click(function(){
		$("input[name='img1']").val(chart1.toBase64Image());	
		$("input[name='img2']").val(chart2.toBase64Image());	
		$("input[name='img3']").val(chart3.toBase64Image());	
		$("input[name='img4']").val(chart4.toBase64Image());	
		$("input[name='val1']").val($('#val-1').html());	
		$("input[name='val2']").val($('#val-2').html());	
		$("input[name='val3']").val($('#val-3').html());	
		$("input[name='val4']").val($('#val-4').html());	
		$("input[name='val5']").val($('#val-5').html());	
		$("input[name='val6']").val($('#val-6').html());	
		$("input[name='val7']").val($('#val-7').html());	
		$("input[name='val8']").val($('#val-8').html());
		$("#form-print").submit();	
	});
	
			
});

function ajaxGeneral() {
	$.ajax({
		async: false,
		url: '/statistics/default/general-data',
		type: 'POST',
		data: {
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()
			
		},
		dataType: 'JSON',
		success: function(data) {
			$('#val-1').html(data['total']);
			$('#val-2').html(data['solved']);
			$('#val-3').html(data['day']);
			$('#val-4').html(data['month']);
			$('#val-5').html(data['types']);
			$('#val-6').html(data['dists']);
			$('#val-7').html(moment(data['peak_day']).format('LL') + " / " + moment(data['peak_month']).format('MMM YYYY'));
			$('#val-8').html(data['organization']);
		}	
	});
}

function ajaxMap() {
	
	if(typeof mymap !== 'undefined') mymap.remove();
	mymap = L.map('mapid').setView([43.6615, 51.15103], 13);
	var info = L.control();
	var geojson;
	var legend = L.control({position: 'bottomright'});
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZnVzZXdpdiIsImEiOiJjajZoaDh3NHcwbG05MnhzY3M1aHRyMWxqIn0.Uv1L9Mz_hfLgKfH32XEbVg', {
	    id: 'mapbox.light',
	    //attribution: ...
	}).addTo(mymap);
	
	$.ajax({
		url: '/statistics/default/ajax-map',
		type: 'POST',
		dataType: 'JSON',
		data: {
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()
		},
		success: function(data) {
			//L.geoJson(data).addTo(mymap);
			geojson = L.geoJson(data, {style: style, onEachFeature: onEachFeature}).addTo(mymap);
			mymap.scrollWheelZoom.disable();
		}	
	});
	
	info.onAdd = function (map) {
	    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
	    this.update();
	    return this._div;
	};
	
	info.update = function (props) {
	    this._div.innerHTML = '<h4>Количество заявок</h4>' +  (props ?
	        '<b>Район: ' + props.name + '</b><br />' + props.density + ' заявок'
	        : 'Наведите курсор на район');
	};
	
	function zoomToFeature(e) {
	    mymap.fitBounds(e.target.getBounds());
	}
	
	function onEachFeature(feature, layer) {
	    layer.on({
	        mouseover: highlightFeature,
	        mouseout: resetHighlight,
	        click: zoomToFeature
	    });
	}

	
	function highlightFeature(e) {
	    var layer = e.target;

	    layer.setStyle({
	        weight: 5,
	        color: '#666',
	        dashArray: '',
	        fillOpacity: 0.7
	    });

	    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
	        layer.bringToFront();
	    }
	    info.update(layer.feature.properties);
	}

	function resetHighlight(e) {
	    geojson.resetStyle(e.target);
	    info.update();
	}

	info.addTo(mymap);
	
	legend.onAdd = function (map) {

	    var div = L.DomUtil.create('div', 'info legend'),
	        grades = [0, 10, 20, 50, 100, 200, 500, 1000],
	        labels = [];

	    // loop through our density intervals and generate a label with a colored square for each interval
	    for (var i = 0; i < grades.length; i++) {
	        div.innerHTML +=
	            '<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
	            grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
	    }

	    return div;
	};

	legend.addTo(mymap);
	
	function getColor(d) {
	    return d > 1000 ? '#800026' :
	           d > 500  ? '#BD0026' :
	           d > 200  ? '#E31A1C' :
	           d > 100  ? '#FC4E2A' :
	           d > 50   ? '#FD8D3C' :
	           d > 20   ? '#FEB24C' :
	           d > 10   ? '#FED976' :
	                      '#FFEDA0';
	}

	function style(feature) {
	    return {
	        fillColor: getColor(feature.properties.density),
	        weight: 2,
	        opacity: 1,
	        color: 'white',
	        dashArray: '3',
	        fillOpacity: 0.7
	    };
	}
	
}

function ajaxChart1() {
	$.ajax({
		async: false,
		url: '/statistics/default/ajax-chart-1',
		type: 'POST',
		dataType: 'JSON',
		data: {
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").add(1, 'day').format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()
		},
		success: function(data) {
			if(typeof chart1 !== 'undefined') chart1.destroy();
			chart1 = new Chart($("#chart1"), data);
		}	
	});
}


function ajaxChart2() {
	$.ajax({
		url: '/statistics/default/ajax-chart-2',
		type: 'POST',
		dataType: 'JSON',
		data: {
			is_mobile: is_mobile,
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()
		},
		success: function(data) {
			if(typeof chart2 !== 'undefined') chart2.destroy();
			chart2 = new Chart($("#chart2"), data);
			chart2.chart.ctx.canvas.removeEventListener('wheel', chart2.zoom._wheelHandler);
		}	
	});
}

function ajaxChart3() {
	$.ajax({
		url: '/statistics/default/ajax-chart-3',
		type: 'POST',
		dataType: 'JSON',
		data: {
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()
		},
		success: function(data) {
			if(typeof chart3 !== 'undefined') chart3.destroy();
			chart3 = new Chart($("#chart3"), data);
			chart3.chart.ctx.canvas.removeEventListener('wheel', chart3.zoom._wheelHandler);
		}	
	});
}

function ajaxChart4() {
	$.ajax({
		url: '/statistics/default/ajax-chart-4',
		type: 'POST',
		dataType: 'JSON',
		data: {
			is_mobile: is_mobile,
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()
		},
		success: function(data) {
			if(typeof chart4 !== 'undefined') chart4.destroy();
			chart4 = new Chart($("#chart4"), data);
			chart4.chart.ctx.canvas.removeEventListener('wheel', chart4.zoom._wheelHandler);
		}	
	});
}

function ajaxListView() {
	$.ajax({
		url: '/statistics/default/ajax-list-view',
		type: 'POST',
		data: {
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()
		},
		success: function(data) {
			$("#list_view").html(data);
		}	
	});
}

