<?php

namespace app\modules\statistics\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;

use app\models\DatesForm;
use app\modules\application\models\Application;
use app\modules\application\models\ApplicationSearch;
use app\modules\application\models\ApplicationTypes;
use app\modules\application\models\ApplicationAssignment;
use app\modules\organization\models\Organization;

/**
 * Default controller for the `statistics` module
 */
class DefaultController extends Controller
{
    public $layout = "@app/modules/workplace/views/layouts/main";
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'moderator', 'controller'],
                    ]
                    
                ],
            ]
        ];
    }
     
    public function actionIndex()
    {        
        $model = new ApplicationSearch;
        $model->date1 = substr(Application::find()->orderBy('date_created ASC')->one()->date_created, 0, -5);
        $model->date2 = substr(Application::find()->orderBy('date_created DESC')->one()->date_created, 0, -5);
        return $this->render('index', ['model'=>$model]);
    }
     
    public function actionStats()
    {        
    	
    	$this->layout = "@app/views/layouts/main";
        $model = new ApplicationSearch;
        $model->date1 = substr(Application::find()->orderBy('date_created ASC')->one()->date_created, 0, -5);
        $model->date2 = substr(Application::find()->orderBy('date_created DESC')->one()->date_created, 0, -5);
        return $this->render('@app/modules/statistics/views/default/stats', ['model'=>$model]);
    }
    
    public function actionStatsUncompleted() {
		
		$this->layout = "@app/views/layouts/main";
		
		return $this->render('@app/modules/statistics/views/default/uncompleted');
	}
    
    // Неисполненные заявки
    public function actionUncompleted() {
		$total = Application::find()->where(['IN', 'status', [0, 1, 2]])->count();
		$checking = Application::find()->where(['=', 'status', 1])->count();
		$working = Application::find()->where(['=', 'status', 2])->count();
		$frozen = Application::find()->where(['=', 'status', 0])->count();
		
		$month_ago_date = date('Y-m-d', strtotime("-30 days"));
		
		$total_month = Application::find()->where(['IN', 'status', [0, 1, 2]])->andWhere(['<=', 'DATE(date_created)', $month_ago_date])->count();
		$checking_month = Application::find()->where(['=', 'status', 1])->andWhere(['<=', 'DATE(date_created)', $month_ago_date])->count();
		$working_month = Application::find()->where(['=', 'status', 2])->andWhere(['<=', 'DATE(date_created)', $month_ago_date])->count();
		$frozen_month = Application::find()->where(['=', 'status', 0])->andWhere(['<=', 'DATE(date_created)', $month_ago_date])->count();
		
		
		$sql = "SELECT uncomplete_reason, count(uncomplete_reason) as count FROM application 
			WHERE DATE(date_created) <= '$month_ago_date' 
			AND status IN (0, 1, 2)
			GROUP BY uncomplete_reason";
		$reasons = Yii::$app->db->createCommand($sql)->queryAll();
		$sql = "SELECT COUNT(*) as count FROM application 
			WHERE DATE(date_created) <= '$month_ago_date' 
			AND status IN (0, 1, 2)
			AND uncomplete_reason IS NULL";
		$not_defined_reason = Yii::$app->db->createCommand($sql)->queryAll();
		$reasons[0] = $not_defined_reason[0];
		$reasons[0]['uncomplete_reason'] = 'Не указано';
		
		$sql = "SELECT name, count(id) as count FROM 
			( SELECT org.name as name, app.id as id 
				FROM organization org 
				INNER JOIN application_assignment apas on org.id = apas.organization_id 
				INNER JOIN application app on apas.application_id = app.id 
				WHERE DATE(app.date_created) <= '$month_ago_date' AND app.status IN (0, 1, 2) GROUP BY app.id 
			) as new_table GROUP BY name";
		$organizations = Yii::$app->db->createCommand($sql)->queryAll();
		
		
		//echo "<pre>"; print_r($not_defined_reason); echo "</pre>"; exit;
		$stats = [
			'total'=>$total, 
			'checking'=>$checking, 
			'working'=>$working, 
			'frozen'=>$frozen,
			'total_month'=>$total_month, 
			'checking_month'=>$checking_month, 
			'working_month'=>$working_month, 
			'frozen_month'=>$frozen_month,
			'reasons' => $reasons,
			'organizations' => $organizations
		];
		
		$models = Application::find()->where(['IN', 'status', [0, 1, 2]]);
		
		return $this->render('@app/modules/statistics/views/default/uncompleted', ['stats'=>$stats, 'models'=>$models]);
	}
    
    public function actionForExcell() {
		$dates = new DatesForm();
		$dates->date1 = date('01.m.Y');
		$dates->date2 = date('t.m.Y');
		$dates->load(Yii::$app->request->post());
		
		$d1 = Yii::$app->formatter->asDate($dates->date1, 'php:Y-m-d');
		$d2 = Yii::$app->formatter->asDate($dates->date2, 'php:Y-m-d');
		
		$applications = Application::find()->where(['BETWEEN', 'DATE(date_created)', $d1, $d2])->all();
		
		$stats['total'] = count($applications);
		$stats['processed'] = 0;
		$stats['processing'] = 0;
		$stats['deleted'] = 0;
		$stats['types'] = [];
		$stats['organizations'] = [];
		
		foreach($applications as $app) {
			if($app->isCompleted()) $stats['processed'] += 1;
			if($app->isProcessing()) $stats['processing'] += 1;
			if($app->isDeleted()) $stats['deleted'] += 1;
			
			if($stats['types'][$app->type->name]) {
				$stats['types'][$app->type->name] ++;
			} else {
				$stats['types'][$app->type->name] = 1;
			}
			
			if($app->getAuthorModel()->isOrganizationUser()) {
				if($stats['organizations'][$app->getAuthorName()]) {
					$stats['organizations'][$app->getAuthorName()] +=1;					
				} else {
					$stats['organizations'][$app->getAuthorName()] = 1;
				}
			} else {
				$stats['organizations']['Пользователи сайта'] +=1;
			}
			
		}
		
		
		return $this->render('for-excell', ['dates'=>$dates, 'applications'=>$applications, 'stats'=>$stats]);
	}
    
    public function actionIndexPrint() {
		$model = new ApplicationSearch;
        $model->date1 = substr(Application::find()->orderBy('date_created ASC')->one()->date_created, 0, -5);
        $model->date2 = substr(Application::find()->orderBy('date_created DESC')->one()->date_created, 0, -5);
        
        
        return $this->renderPartial('index_print', ['model'=>$model]);
	}
    
    public function actionByType() {
		$model = new ApplicationSearch;
        $model->date1 = substr(Application::find()->orderBy('date_created ASC')->one()->date_created, 0, -5);
        $model->date2 = substr(Application::find()->orderBy('date_created DESC')->one()->date_created, 0, -5);
        
        return $this->render('by-type', ['model'=>$model]);
	}
    
    public function actionGeneralData() {
    	Yii::$app->db->createCommand("SET SESSION sql_mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'")->execute();
    	$additionalSql = ($_POST['type']?" AND application_type IN (".implode(",", $_POST['type']).")":"");
    	$additionalSql .= ($_POST['addr']?" AND address IN (".implode(",", $_POST['addr']).")":"");
		
		$result['total'] = Yii::$app->db->createCommand(
			"SELECT COUNT(*) as total FROM application  
				WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' $additionalSql"
		)->queryAll()[0]['total'];
		$result['solved'] = Yii::$app->db->createCommand(
			"SELECT COUNT(*) as total FROM application   
				WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' AND status = 3 $additionalSql")->queryAll()[0]['total'];
		//$date_first = Yii::$app->db->createCommand("SELECT date_created FROM application  WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' ORDER BY date_created ASC LIMIT 1")->queryAll()[0]['date_created'];
		//$date_last = Yii::$app->db->createCommand("SELECT date_created FROM application  WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' ORDER BY date_created DESC LIMIT 1")->queryAll()[0]['date_created'];
		$days = (strtotime($_POST['date2']) - strtotime($_POST['date1'])) / 86400;
		$result['day'] = intval($result['total'] / $days);
		$result['month'] = intval($result['total'] / ($days / 30));
		$types = Yii::$app->db->createCommand("SELECT apt.name_ru as name, COUNT(application_type)
					as count, (COUNT(application_type) / {$result['total']} * 100) as percent FROM application app
					INNER JOIN application_types apt on apt.id = app.application_type
					WHERE DATE(app.date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' $additionalSql
					GROUP BY application_type ORDER BY COUNT(application_type) DESC LIMIT 3")->queryAll();
					
		foreach($types as $type) {
			$type['percent'] = intval($type['percent']) . '%';
			$result['types'].= "{$type['name']} - {$type['percent']}, ";
		}
		$result['types'] = substr($result['types'], 0, -2);
		$dists = Yii::$app->db->createCommand("SELECT address as name, COUNT(address) as count, (COUNT(address) / {$result['total']} * 100) as percent 
					FROM application app  WHERE DATE(app.date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' $additionalSql
					GROUP BY address ORDER BY count(address) DESC LIMIT 3
		")->queryAll();
		foreach($dists as $dist) {
			$dist['percent'] = intval($dist['percent']) . '%';
			$result['dists'].= "{$dist['name']} - {$dist['percent']}, ";
		}
		$result['dists'] = substr($result['dists'], 0, -2);
		
		$orgs = Yii::$app->db->createCommand("SELECT org.name, COUNT(org.id) as count, (COUNT(org.id) / {$result['total']} * 100) as percent FROM application app
					INNER JOIN application_assignment apas on apas.application_id = app.id
					INNER JOIN organization org on org.id = apas.organization_id
					WHERE DATE(app.date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' $additionalSql
					GROUP BY org.id ORDER BY count(org.id) DESC LIMIT 3
		")->queryAll();
		foreach($orgs as $org) {
			$org['percent'] = intval($org['percent']) . '%';
			$result['organization'].= "{$org['name']} - {$type['percent']}<br/>";
		}
		$result['organization'] = substr($result['organization'], 0, -5);
		
		$result['peak_day'] = Yii::$app->db->createCommand("SELECT COUNT(DATE(date_created)), DATE(date_created) as peak FROM application  WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' $additionalSql GROUP BY DATE(date_created) ORDER BY count(DATE(date_created)) DESC")->queryAll()[0]['peak'];
		$result['peak_month'] = Yii::$app->db->createCommand("SELECT date_created as peak FROM application WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' $additionalSql GROUP BY DATE_FORMAT(date_created, '%Y%m') ORDER BY count(DATE_FORMAT(date_created, '%Y%m')) DESC")->queryAll()[0]['peak'];
		
		//echo "<pre>"; print_r($result); echo "</pre>"; exit;
		echo json_encode($result);
	}
	
	public function actionAjaxChart1() {
		Yii::$app->db->createCommand("SET SESSION sql_mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'")->execute();
    	$additionalSql = ($_POST['type']?" AND application_type IN (".implode(",", $_POST['type']).")":"");
    	$additionalSql .= ($_POST['addr']?" AND address IN (".implode(",", $_POST['addr']).")":"");
		$sql = "SELECT Date(date_created) as date, COUNT(Date(date_created)) as count 
			FROM application WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' $additionalSql
			GROUP BY Date(date_created) ORDER BY date_created";		
		$result = Yii::$app->db->createCommand($sql)->queryAll();
		
		$sql2 = "SELECT DATE_FORMAT(date_created, '%Y-%m') as x, COUNT(id) as 'y' 
			FROM application  WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' $additionalSql
			GROUP BY DATE_FORMAT(date_created, '%Y%m')";
		$result2 = Yii::$app->db->createCommand($sql2)->queryAll();
		//Заполнить нулями дни без заявок
		
		foreach($result as $item) {
			$dates_range[] = $item['date'];
			$columns[$item['date']] = [
				'x' => $item['date'],
				'y' => $item['count'],
			];
		}
		
		$dates_range = $this->fillEmptyDays($dates_range, $columns);		
		
		$config = [
			'type' => 'line',
			'data' => [
				//'labels' => $dates,
				'datasets' => [
					[
						'label' => Yii::t('app', 'Заявок в день'),
						'data' => $dates_range,
						'backgroundColor' => [
							$this->getColor(0),
						]
					],
					
					[
						'label' => Yii::t('app', 'Заявок в месяц'),
						'data' => $result2,
						'backgroundColor' => [
							$this->getColor(4),
						]
					]
				]
			],
			'options' => [
				'responsive' => true,
				'maintainAspectRatio' => false,
				'title' => [
					'display' => true,
					//'text' => Yii::t('app', 'Заявки за все время')
				],
				'scales' => [
					'xAxes' => [[
						'type' => 'time',
						'gridLines' => [
							
							'drawOnChartArea' => false,
							'offsetGridLines' => true,
						],
						'time' => [							
							'format' => 'YYYY-MM-DD',
							'parser' => false,
							'tooltipFormat' => 'Do MMM YYYY',
							'minUnit' => 'day'
						],
						'scaleLabel' => [
							'display' => false
						],
						'ticks' => [
							'maxRotation' => 0,
							'mirror' => true,
							'padding' => 120
						]
					],[
						'type' => 'time',
						'time' => [							
							'format' => 'YYYY-MM',
							'parser' => false,
							'tooltipFormat' => 'YYYY-MM',
							'maxUnit' => 'month',
							'minUnit' => 'month'
						],
						'ticks' => [
							'maxRotation' => 0,
							'mirror' => true,
							'padding' => 120
						]
					]],
					'yAxes' => [[
						'scaleLabel' => [
							'display' => true,
							'labelString' => Yii::t('app', 'Количество заявок')
						]
					]]
				],
				'zoom' => [
					'enabled' => true,
					'drag' => false,
					'mode' => 'x'
				],
				'pan' => [
					'enabled' => true,
					'mode' => 'x',
					'threshold' => 1
				]
			]
		];
		
		print_r(json_encode($config));
	}
	
	public function actionAjaxChart2() {
    	$additionalSql = ($_POST['type']?" AND application_type IN (".implode(",", $_POST['type']).")":"");
    	$additionalSql .= ($_POST['addr']?" AND address IN (".implode(",", $_POST['addr']).")":"");
		
		$sql = "select apt.name_ru as x, COUNT(application_type) as y from application a 
			inner JOIN application_types apt on a.application_type = apt.id 
			WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' $additionalSql
			GROUP BY application_type
			ORDER BY COUNT(application_type) DESC";
		$result = Yii::$app->db->createCommand($sql)->queryAll();
		$colors = [];
		foreach($result as $res) {
			$labels[] = $res['x'];
			$vals[] = intval($res['y']);
			$colors[] = $this->getColor(rand());
		}
		
		//echo "<pre>"; print_r($_POST['is_mobile'] ); echo "</pre>"; exit;
		
		$config = [
			'type' => ($_POST['is_mobile'] === 'true') ? 'horizontalBar' : 'bar',
			'data' => [
				'labels' => $labels,
				'datasets' => [
					['label' => 'Количество заявок', 'data' => $vals, 'backgroundColor' =>$colors],
				]
			],
			'options' => [
				'responsive' => true,
				'maintainAspectRatio' => false,
				'title' => [
					'display' => true,
					'text' => Yii::t('app', 'Статистика по типам')
				],
				'scales' => [
					'yAxes' => [[
						'scaleLabel' => [
							'display' => true,
							'labelString' => Yii::t('app', 'Количество заявок')
						],
						'ticks' => [
							'min' => 0,
							'max' => max($vals) + 10,
							'mirror' => true
						]
					]]
				]
			]
			
		];
		
		echo json_encode($config);
		
	}
	
	public function actionAjaxChart3() {
    	$additionalSql = ($_POST['type']?" AND application_type IN (".implode(",", $_POST['type']).")":"");
    	$additionalSql .= ($_POST['addr']?" AND address IN (".implode(",", $_POST['addr']).")":"");
		
		$sql = "select ap.status as x, count(ap.status) as y from application ap 
			WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' $additionalSql
			GROUP BY ap.status";
		$result = Yii::$app->db->createCommand($sql)->queryAll();
		foreach($result as $key=>$val) {
			$result[$key]['x'] = Yii::$app->params['application']['status'][$val['x']];
		}
		foreach($result as $res) {
			$labels[] = $res['x'];
			$vals[] = intval($res['y']);
		}
		$config = [
			'type' => 'pie',
			'data' => [
				'labels' => $labels,
				'datasets' => [
					[
						'label' => 'Количество заявок', 
						'data' => $vals,
						'backgroundColor' => [
							'rgba(43,81,125, .5)',
							'rgba(255,127,1, .5)',
							'rgba(55,255,1, .5)',
							'rgba(246,255,1, .5)',
						],
					]
				]
			],
			'options' => [
				'responsive' => true,
				'maintainAspectRatio' => false,
				'title' => [
					'display' => true,
					'text' => Yii::t('app', 'Статистика по текущему статусу')
				]
			]
			
		];
		
		echo json_encode($config);
		
	}
	
	public function actionAjaxChart4() {
    	$additionalSql = ($_POST['type']?" AND application_type IN (".implode(",", $_POST['type']).")":"");
    	$additionalSql .= ($_POST['addr']?" AND address IN (".implode(",", $_POST['addr']).")":"");
		
		$sql = "SELECT org.name as x, COUNT(apas.organization_id) as y FROM application ap 
			INNER JOIN application_assignment apas ON ap.id = apas.application_id
			INNER JOIN organization org ON org.id = apas.organization_id
			WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' 
			AND '{$_POST['date2']}' 
			AND ap.status = 3
			$additionalSql
			GROUP BY apas.organization_id
			ORDER BY COUNT(apas.organization_id) DESC";
		$result = Yii::$app->db->createCommand($sql)->queryAll();
		
		$uncompleted = "SELECT org.name as x, COUNT(apas.organization_id) as y FROM application ap 
			INNER JOIN application_assignment apas ON ap.id = apas.application_id
			INNER JOIN organization org ON org.id = apas.organization_id
			WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' 
				AND '{$_POST['date2']}'
				AND ap.status IN (0, 1, 2)
				$additionalSql
			GROUP BY apas.organization_id
			ORDER BY COUNT(apas.organization_id) DESC";
		$uncompleted_result = Yii::$app->db->createCommand($uncompleted)->queryAll();
		
		$colors = [];
		foreach($result as $res) {
			$labels[] = str_replace('"', '', $res['x']);
			$vals[] = intval($res['y']);
			//$colors[] = $this->getColor(rand());
		}
		foreach($uncompleted_result as $res) {
			$uncompleted_vals[] = intval($res['y']);
			//$uncompleted_colors[] = $this->getColor(rand());
		}
		$config = [
			'type' => 'horizontalBar',
			'data' => [
				'labels' => $labels,
				'datasets' => [
					['label' => Yii::t('app', 'Исполнено'), 'data' => $vals, 'backgroundColor' =>'rgba(43, 81, 125, .7)'],
					['label' => Yii::t('app', 'В работе'), 'data' => $uncompleted_vals, 'backgroundColor' =>'rgba(43, 81, 125, .3)']
				]
			],
			'options' => [
				'responsive' => true,
				'maintainAspectRatio' => false,
				
				'title' => [
					'display' => true,
					'text' => Yii::t('app', 'Статистика по адресованым заявкам')
				],
				'scales' => [
					'yAxes' => [[
						'stacked' => true,
						'ticks' => [
							'min' => 0,
							'mirror' => true
						]
					]],
					'xAxes' => [[
						'stacked' => true,
						'ticks' => [
							'min' => 0,
							'autoSkip' => false
						]
					]],
					
				]
			]
			
		];
		
		echo json_encode($config);
		
	}
	
	public function actionAjaxMap() {
    	$additionalSql = ($_POST['type']?" AND application_type IN (".implode(",", $_POST['type']).")":"");
    	$additionalSql .= ($_POST['addr']?" AND address IN (".implode(",", $_POST['addr']).")":"");
		
		//$sql = "SELECT ST_ASTEXT(geometry) as geometry, name FROM gis_areas";
		$sql = "SELECT COUNT(ap.address) as value, ga.name as name, ST_ASTEXT(ga.geometry) as geometry
			FROM application ap 
			INNER JOIN gis_areas ga on ga.id = ap.address
			WHERE DATE(ap.date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' $additionalSql
			GROUP BY ap.address";
		$result = Yii::$app->db->createCommand($sql)->queryAll();
		//echo "<pre>"; print_r($result); echo "</pre>"; exit;
		foreach($result as $data) {
			
			$data['geometry'] = str_replace('POLYGON((','', $data['geometry']);
			$data['geometry'] = str_replace(')','', $data['geometry']);
			$data['geometry'] = explode(',', $data['geometry']);
			
			foreach($data['geometry'] as $key=>$val) {
				$data['geometry'][$key] = explode(' ', $val);
			}
			
			$features[] = [
				'type' => 'Feature',
				'properties' => [
					'name' => $data['name'],
					'density' => $data['value']
				],
				'geometry' => [
					'type' => 'Polygon',
					'coordinates' => [$data['geometry']]
				]	
			];
		}
		
		$data = [
			'type' => 'FeatureCollection',
			'crs' => [
				'type' => 'name',
				'properties' => [
					'name' => 'urn:ogc:def:crs:OGC:1.3:CRS84'
				]
			], 
			'features' => $features
		];
		echo json_encode($data);
	}
	
	public function actionAjaxListView() {
		$additionalSql = ($_POST['type']?" AND application_type IN (".implode(",", $_POST['type']).")":"");
    	$additionalSql .= ($_POST['addr']?" AND address IN (".implode(",", $_POST['addr']).")":"");
		$sql = "SELECT id, date_created, application_type 
			FROM application WHERE DATE(date_created) BETWEEN '{$_POST['date1']}' AND '{$_POST['date2']}' $additionalSql
			ORDER BY date_created DESC";
		
		$query = Application::findBySql($sql);
		$dp = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
			]/*,
			'sort' => [
				'defaultOrder' => [
					'date_human' => SORT_DESC
				],
				'attributes' => [
					'date_human' => [
						'desc' => ['date'=>SORT_DESC],
						'asc' => ['date'=>SORT_ASC],
					]
				]
			]*/
		]);
		
		return $this->renderAjax('_list_view', ['dp'=>$dp]);
		
	}
	
	public function actionPrint() {
		
		$data = $_POST;
		//echo "<pre>"; print_r($data); echo "</pre>"; exit;
		$model = new ApplicationSearch;
        $model->date1 = substr(Application::find()->orderBy('date_created ASC')->one()->date_created, 0, -5);
        $model->date2 = substr(Application::find()->orderBy('date_created DESC')->one()->date_created, 0, -5);
		
		$pdf = new Pdf([
	        'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
	        'content' => $this->renderPartial('index_print', ['model'=>$model, 'data'=>$data]),
	        'cssFile' => '@app/modules/statistics/assets/css/print.css',
	        'options' => [
	            'title' => 'Privacy Policy - Krajee.com',
	            'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
	        ],
	        'methods' => [
	            'SetHeader' => ['Generated By: Krajee Pdf Component||Generated On: ' . date("r")],
	            'SetFooter' => ['|Page {PAGENO}|'],
	        ]
	    ]);
	    return $pdf->render();
	    //return $this->renderPartial('index_print', ['model'=>$model, 'data'=>$data]);
	}
	
	public function fillEmptyDays($dates_range, $columns) {
		$min_date = $dates_range[0];
		$max_date = end($dates_range);
		$dates_range = [];
		$period = new \DatePeriod(
		     new \DateTime($min_date),
		     new \DateInterval('P1D'),
		     new \DateTime($max_date)
		);
		foreach($period as $dt) {
			$dates_range[]['x'] = $dt->format('Y-m-d');
		}
		foreach($dates_range as $key=>$val) {
			if($columns[$val['x']]) {
				$dates_range[$key]['y'] = $columns[$val['x']]['y'];
			} else {
				$dates_range[$key]['y'] = 0;
			}
		}
		return $dates_range;
	}
	
	public function actionAjaxUncompleted1() {
		$total = Application::find()->where(['IN', 'status', [0, 1, 2]])->count();
		$checking = Application::find()->where(['=', 'status', 1])->count();
		$working = Application::find()->where(['=', 'status', 2])->count();
		$frozen = Application::find()->where(['=', 'status', 0])->count();
		
		$config = [
			'type' => 'doughnut',
			'data' => [
				'labels' => [
					Yii::t('app', 'Проверяется {0}', [$checking]), 
					Yii::t('app', 'В работе {0}', [$working]), 
					Yii::t('app', 'Заморожено {0}', [$frozen]), 
					],
				'datasets' => [
					[
						'data' => [
							$checking, $working, $frozen
						],
						'backgroundColor' => [
							'rgba(210,220,1, .5)',
							'rgba(37,223,1, .5)',
							'rgba(43,81,125, .5)'
						],
					]
				]
			],
			'options' => [
				'responsive' => true,
				'maintainAspectRatio' => false,
				'title' => [
					'display' => true,
					'text' => Yii::t('app', 'Неисполненные за все время')
				],
				'elements'=>[
					'center' => [
						'text' => $total . " за все время"
					]
				]
			]
			
		];
		echo json_encode($config);
		
	}
	public function actionAjaxUncompleted2() {
		$month_ago_date = date('Y-m-d', strtotime("-30 days"));		
		$total_month = Application::find()->where(['IN', 'status', [0, 1, 2]])->andWhere(['<=', 'DATE(date_created)', $month_ago_date])->count();
		$checking_month = Application::find()->where(['=', 'status', 1])->andWhere(['<=', 'DATE(date_created)', $month_ago_date])->count();
		$working_month = Application::find()->where(['=', 'status', 2])->andWhere(['<=', 'DATE(date_created)', $month_ago_date])->count();
		$frozen_month = Application::find()->where(['=', 'status', 0])->andWhere(['<=', 'DATE(date_created)', $month_ago_date])->count();
		
		$config = [
			'type' => 'doughnut',
			'data' => [
				'labels' => [
					Yii::t('app', 'Проверяется {0}', [$checking_month]), 
					Yii::t('app', 'В работе {0}', [$working_month]), 
					Yii::t('app', 'Заморожено {0}', [$frozen_month]), 
					],
				'datasets' => [
					[
						'data' => [
							$checking_month, $working_month, $frozen_month
						],
						'backgroundColor' => [
							'rgba(210,220,1, .5)',
							'rgba(37,223,1, .5)',
							'rgba(43,81,125, .5)'
						],
					]
				]
			],
			'options' => [
				'responsive' => true,
				'maintainAspectRatio' => false,
				'title' => [
					'display' => true,
					'text' => Yii::t('app', 'Неисполненные более 30 дней')
				],
				'elements'=>[
					'center' => [
						'text' => $total_month . " более месяца"
					]
				]
			]
			
		];
		echo json_encode($config);
		
	}
	
	public function actionAjaxUncompleted3() {
    	$month_ago_date = date('Y-m-d', strtotime("-30 days"));
    	$additionalSql = ($_POST['type']?" AND application_type IN (".implode(",", $_POST['type']).")":"");
    	$additionalSql .= ($_POST['addr']?" AND address IN (".implode(",", $_POST['addr']).")":"");
		
		$sql = "SELECT uncomplete_reason as x, count(uncomplete_reason) as y FROM application 
			WHERE DATE(date_created) <= '$month_ago_date' 
			AND status IN (0, 1, 2)
			GROUP BY uncomplete_reason";
		$reasons = Yii::$app->db->createCommand($sql)->queryAll();
		$sql = "SELECT COUNT(*) as y FROM application 
			WHERE DATE(date_created) <= '$month_ago_date' 
			AND status IN (0, 1, 2)
			AND uncomplete_reason IS NULL";
		$not_defined_reason = Yii::$app->db->createCommand($sql)->queryAll();
		$reasons[0] = $not_defined_reason[0];
		$reasons[0]['x'] = 'Не указано';
		
		$colors = [];
		foreach($reasons as $res) {
			$labels[] = str_replace('"', '', $res['x']);
			$vals[] = intval($res['y']);
			$colors[] = $this->getColor(rand());
		}
		$config = [
			'type' => 'horizontalBar',
			'data' => [
				'labels' => $labels,
				'datasets' => [
					['label'=>'', 'data' => $vals, 'backgroundColor' =>$colors]
				]
			],
			'options' => [
				'responsive' => true,
				'maintainAspectRatio' => false,
				
				'title' => [
					'display' => true,
					'text' => Yii::t('app', 'Неисполненные более 30 дней, причины')
				],
				'scales' => [
					'yAxes' => [[
						'ticks' => [
							'min' => 0,
							'mirror' => true
						]
					]],
					'xAxes' => [[
						'ticks' => [
							'min' => 0,
							'autoSkip' => false
						]
					]],
					
				]
			]
			
		];
		
		echo json_encode($config);
		
	}
	
	public function actionAjaxUncompleted4() {
    	$month_ago_date = date('Y-m-d', strtotime("-30 days"));
    	$additionalSql = ($_POST['type']?" AND application_type IN (".implode(",", $_POST['type']).")":"");
    	$additionalSql .= ($_POST['addr']?" AND address IN (".implode(",", $_POST['addr']).")":"");
		
		$sql = "SELECT name as x, count(id) as y FROM 
			( SELECT org.name as name, app.id as id 
				FROM organization org 
				INNER JOIN application_assignment apas on org.id = apas.organization_id 
				INNER JOIN application app on apas.application_id = app.id 
				WHERE DATE(app.date_created) <= '$month_ago_date' AND app.status IN (0, 1, 2) GROUP BY app.id 
			) as new_table GROUP BY name ORDER BY count(id) DESC";
		$organizations = Yii::$app->db->createCommand($sql)->queryAll();
		
		$colors = [];
		foreach($organizations as $res) {
			$labels[] = str_replace('"', '', $res['x']) . " - " . $res['y'];
			$vals[] = intval($res['y']);
			$colors[] = $this->getColor(rand());
		}
		$config = [
			'type' => 'horizontalBar',
			'data' => [
				'labels' => $labels,
				'datasets' => [
					['label'=>'', 'data' => $vals, 'backgroundColor' =>$colors]
				]
			],
			'options' => [
				'responsive' => true,
				'maintainAspectRatio' => false,
				
				'title' => [
					'display' => true,
					'text' => Yii::t('app', 'Неисполненные более 30 дней, организации')
				],
				'scales' => [
					'yAxes' => [[
						'ticks' => [
							'min' => 0,
							'mirror' => true
						]
					]],
					'xAxes' => [[
						'ticks' => [
							'min' => 0,
							'autoSkip' => false
						]
					]],
					
				]
			]
			
		];
		
		echo json_encode($config);
		
	}
	
	public function actionTest() {
		$models = Application::find()->select('id')->indexBy('id')->column();;
		$executors = Organization::find()->select('id')->indexBy('id')->column();
		//echo "<pre>"; print_r($models); echo "</pre>"; exit;
		foreach($models as $md) {
			$model = new ApplicationAssignment;
			$model->application_id = $md;
			$model->organization_id = array_rand($executors);
			$model->application_end = '2017-12-31';
			$model->save();
			//echo "<pre>"; print_r($executors); echo "</pre>"; exit;
			//$md->update_address();
		}
	}
    
    public function actionFill() {
		
		$statuses = [-1, 0, 1, 2, 3];
		$types = ApplicationTypes::find()->where(['!=', 'parent_id', 0])->select('id')->indexBy('id')->column();
		
		for($i = 0; $i < 1000; $i++) {
			$model = new Application;
			$model->date_created = $this->rand_date('2017-01-01', '2017-12-30');
			$model->date_plane_end = date('d.m.Y H:i:s', strtotime(Yii::$app->params['application']['days_to_add']));
			$model->author = Yii::$app->user->id;
			$model->active = 1;
			$model->status = array_rand($statuses);
			$model->application_type = array_rand($types);
			$model->description = "Автоматическое наполнение";
			$points = 1000000;
			$model->coords = (rand(43.641674*$points, 43.679186*$points) / $points) . "," . (rand(51.128103*$points, 51.201917*$points) / $points);
			
			$model->save();
		}
		//echo "<pre>"; print_r($result); echo "</pre>"; exit;
	}
	
	function rand_date($min_date, $max_date) {
	    $min_epoch = strtotime($min_date);
	    $max_epoch = strtotime($max_date);

	    $rand_epoch = rand($min_epoch, $max_epoch);

	    return date('Y-m-d H:i:s', $rand_epoch);
	}
	
	function getColor($num) {
	    $hash = md5('color' . $num); // modify 'color' to get a different palette
	    return "rgba(".hexdec(substr($hash, 0, 2)).", ". hexdec(substr($hash, 2, 2)).",".hexdec(substr($hash, 4, 2)).", .5)";
	}
	
    
}
