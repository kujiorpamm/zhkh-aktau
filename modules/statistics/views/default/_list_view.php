<?php
	use yii\helpers\Html;
	use kartik\grid\GridView;
?>

<?=GridView::widget([
		    'dataProvider'=> $dp,
		    'columns' => [
		    		'id',
		    		'date_created',
		    		[
		    			'attribute' => 'application_type',
		    			'value' => function($model) {
							return $model->subtype->name_ru;
						}
		    		],
		    		[
		    			'format' => 'raw',
		    			'attribute' => 'link',
		    			'value' => function($model) {
							return $model->link;
						}
		    		]
		    	],
		    'responsive'=>true,
		    'hover'=>true,
		    'striped'=>false,
		    'pjax'=>false,
		    'toolbar'=> false,
		    'export'=>[
		        'fontAwesome'=>false,
		        'options' => [
		        	'class' => 'btn-export',
		        ]
		    ],
		    'exportConfig'=> [
				GridView::EXCEL => true
			],
		    'panel'=>[
	        	'type'=>GridView::TYPE_DEFAULT,
	        	'footer' => false
		    ]
		]);?>