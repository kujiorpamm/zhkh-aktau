function resetPassword(message) {
	event.preventDefault();
	if(confirm(message)) {
		
		$.ajax({
			url: "/me/reset-password",
			type: "POST",
			dataType: "JSON",
			success: function(data) {
				alert(data.message);
			}	
		});
		
	}
}

function resetPasswordTo(message, id) {
	event.preventDefault();
	if(confirm(message)) {
		
		$.ajax({
			url: "/user/admin/reset-password",
			type: "POST",
			dataType: "JSON",
			data: {
				id: id
			},
			success: function(data) {
				alert(data.message);
			}	
		});
		
	}
}