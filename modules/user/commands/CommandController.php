<?php

namespace app\modules\user\commands;
use yii\console\controllers;
use yii\helpers\Console;

class CommandController extends \yii\console\Controller
{
    public function actionIndex()
    {
        $auth = \Yii::$app->authManager;
		
		$admin = $auth->createRole('admin');
		$admin->description = 'Администратор';
		$auth->add($admin);
		
		$user = $auth->createRole('moderator');
		$user->description = 'Модератор';
		$auth->add($user);
		
		/*ОРГАНИЗАЦИИ*/
		$organization = $auth->createRole('organization');
		$organization->description = 'Организация';
		$auth->add($organization);
		
		$maek = $auth->createRole('maek');
		$maek->description = 'МАЭК';
		$maek->description = 'МАЭК';
		$auth->add($maek);
		$auth->addChild($maek, $organization);
		
		$ouok = $auth->createRole('ouok');
		$ouok->description = 'ОУОК';
		$auth->add($ouok);
		$auth->addChild($ouok, $organization);
		
		$controller = $auth->createRole('controller');
		$controller->description = 'Контролирующая организация';
		$auth->add($controller);
		$auth->addChild($controller, $organization);		
		/*КОНЕЦ ОРГАНИЗАЦИИ*/
		
		$user = $auth->createRole('user');
		$user->description = 'Пользователь';
		$auth->add($user);
		
		$auth->assign($admin, 1);
		
    }
}

?>