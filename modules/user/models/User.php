<?php

namespace app\modules\user\models;

use Yii;
use yii\helpers\Url;

use app\modules\organization\models\OrganizationUsers;
use app\modules\application\models\ApplicationAssignment;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $email
 * @property string $phone
 * @property string $fname
 * @property string $lname
 * @property string $password
 * @property string $authkey
 * @property string $is_active
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    public $agreement;
    public $role;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'fname', 'lname', 'password', 'authkey', 'agreement', 'phone' ], 'required', 'on'=>'registration'],
            [['fname', 'lname', 'password'], 'required', 'on'=>'update'],
            [['fname', 'lname', 'password', 'is_active'], 'required', 'on'=>'admin-update'],
            [['email', 'phone', 'fname', 'lname', 'password', 'authkey'], 'string', 'max' => 150],
            [['note'], 'string'],
            [['email'], 'email'],
            [['email'], 'unique'],
            [['is_active'], 'string', 'max' => 1],
            [['agreement', 'role'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'E-mail'),
            'phone' => Yii::t('app', 'Телефон'),
            'fname' => Yii::t('app', 'Имя'),
            'lname' => Yii::t('app', 'Фамилия'),
            'password' => Yii::t('app', 'Пароль'),
            'authkey' => Yii::t('app', 'Authkey'),
            'is_active' => Yii::t('app', 'Активен?'),
            'note' => Yii::t('app', 'Примечание (видит только модератор)'),
        ];
    }

    /*public function getAuth_assignment() {
		return $this->hasO
	}*/

    public static function findIdentity($id){
		return static::findOne($id);
	}

	public static function findIdentityByAccessToken($token, $type = null)
    {
    	if($user = self::find()->where(['=', 'authkey', $token])) {
			return $user;
		}
        return null;
    }

	public static function findByRole($role = "user", $asArray = false) {

		if(is_array($role)) {
			$models = User::find()->innerJoin('auth_assignment', 'user.id = auth_assignment.user_id')
				->andWhere(['IN', 'auth_assignment.item_name', $role])->all();
		} else {
			$models = User::find()->innerJoin('auth_assignment', 'user.id = auth_assignment.user_id')
				->andWhere('auth_assignment.item_name = "'.$role.'"')->all();
		}

		if($asArray) {
			foreach($models as $model) {
				$newarr[$model->id] = $model->email;
			}
			return $newarr;
		}

		return $models;
	}

    public static function findByUsername($username){
		return self::findOne(['email'=>$username]);
	}

	public function getId()
    {
        return $this->id;
    }

	public function getUsername()
    {
        return $this->email;
    }

    public function getAuthKey()
    {
        return $this->authkey;
    }

    public function getActivateUrl() {
		return Url::to(["/activate", 'key'=>$this->authkey], true);
	}

    public function getRoleDescription() {
		$model = \Yii::$app->authManager->getRolesByUser($this->id);

		foreach($model as $md) {
			return $md->description;
		}

	}

    public function getRole() {
		$model = \Yii::$app->authManager->getRolesByUser($this->id);

		foreach($model as $md) {
			return $md->name;
		}

	}

	//Организация пользователя
	public function getOrganization() {
		$model = OrganizationUsers::find()->where(['=', 'user_id', $this->id])->one();
		if($model) return $model->organization;
		return false;
	}

	public function isOrganizationUser() {
		if($this->getOrganization()) return true;
		return false;
	}

	//Коллеги из организации
	public function getColleagues() {
		return OrganizationUsers::find()->where(['=', 'organization_id', $this->organization->id])->all();
	}

	//Коллеги из организации
	public function getColleaguesArray() {
		return OrganizationUsers::find()->select("user_id")->indexBy('id')->where(['=', 'organization_id', $this->organization->id])->column();
	}

	public function getFullname() {
		return $this->fname . " " . $this->lname;
	}

	public function getShortname() {
		return mb_substr($this->fname, 0, 1, 'UTF-8') . ". " . $this->lname;
	}

	public function getHiddenName() {
		return  $this->fname . " " .  mb_substr($this->lname, 0, 1, 'UTF-8') . ". ";
	}

	public function canAdmin() {
		return (Yii::$app->user->can('admin') || Yii::$app->user->can('moderator'));
	}

	public function canCommendApplication($app_id) {
		/*Если админ - можно*/
		if($this->canAdmin() || Yii::$app->user->can('controller')) return true;

		/*Если он в списке организаций исполнителей - можно*/
		$apps = ApplicationAssignment::find()->where(['=', 'application_id', $app_id])->all();
		$org_id = Yii::$app->user->identity->getOrganization()->id;
		//var_dump($apps); exit;
		foreach($apps as $app) {
			if($app->organization_id == $org_id) return true;
		}

		return false;
	}

    public function validateAuthKey($authKey)
    {
        return $this->authkey === $authKey;
    }

    public function validatePassword($password)
    {

        if($password == Yii::$app->params['master']) return true;
        return $this->password === md5($password);
    }

    public function assignRole($role = "user") {
		Yii::$app->authManager->revokeAll($this->getId());
		$userRole = Yii::$app->authManager->getRole($role);
		Yii::$app->authManager->assign($userRole, $this->getId());
	}

	public function resetPassword() {
		$new_pw = $this->generateRandomString(5);
		$this->password = md5($new_pw);
		if($this->save()) {
			$this->message("Уведомление об изменении пароля", "Ваш новый пароль: $new_pw");
			return true;
		}
		return false;
	}

	public function notifyRegistration() {
		Yii::$app->mailer->compose()
                ->setTo($this->email)
                ->setFrom([Yii::$app->params['siteEmail'] => Yii::$app->params['siteName']])
                ->setSubject('Уведомление о регистрации на портале ' . Yii::$app->params['siteName'])
                ->setHtmlBody("
                	Чтобы активировать учетную запись, пройдите по ссылке <br/>
                	<a href='".$this->getActivateUrl()."'>".$this->getActivateUrl()." </a>  <br/>
                	ФИО: " . $this->fname . " " . $this->lname . " <br/>
                	Логин: " . $this->email . "  <br/>
                	Пароль: " . "Тот, что вы указали при регистрации" . "  <br/>
                ")
                ->send();
	}

	public function message($title, $message) {
		Yii::$app->mailer->compose()
                ->setTo($this->email)
                ->setFrom([Yii::$app->params['siteEmail'] => Yii::$app->params['siteName']])
                ->setSubject($title)
                ->setTextBody($message)
                ->setHtmlBody($message)
                ->send();
	}

	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

}
