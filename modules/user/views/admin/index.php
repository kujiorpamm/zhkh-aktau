<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Пользователи');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="workplace">
	
	<div class="heading">
		<h3><?= Html::encode($this->title) ?></h3>
		
	</div>
	
	<div class="application-container">
		<?= GridView::widget([
	        'dataProvider' => $dataProvider,
	        'filterModel' => $searchModel,
	        'columns' => [
	            //['class' => 'yii\grid\SerialColumn'],

	            [
	            	'attribute'=>'id',
	            	'headerOptions' => ['style'=>'width: 80px;']
	            ],
	            'email',
	            'phone',
	            'fname',
	            'lname',
	            // 'password',
	            // 'authkey',
	            // 'is_active',

	            [
	            	'class' => 'yii\grid\ActionColumn',
	            	'template' => '{view}',
	            	'urlCreator' => function ($action, $model, $key, $index) {
			            if ($action === 'view') {
			                $url ='/user/admin/view-user?id='.$model->id;
			                return $url;
			            }
			        }
	            	
	            ],
	        ],
	    ]); ?>
	</div>
	
	
</div>
