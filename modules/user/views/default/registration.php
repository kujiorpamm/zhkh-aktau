<?php
use yii\bootstrap\ActiveForm;

?>

<div id="page-registration">

	<div class="form-container">
		<p><?=Yii::t('app', 'Пожалуйста, укажите настоящие данные, чтобы наши операторы могли связаться с вами, в случае необходимости.')?></p>
		<br/>
		
		<?php $form = ActiveForm::begin(); ?>
		<?= $form->errorSummary($model); ?>

		<?=$form->field($model, 'fname')->textInput(['placeholder'=>$model->getAttributeLabel('fname')])->label(false)?>
		<?=$form->field($model, 'lname')->textInput(['placeholder'=>$model->getAttributeLabel('lname')])->label(false)?>
		<?=$form->field($model, 'email')->textInput(['placeholder'=>$model->getAttributeLabel('email')])->label(false)?>
		<?=$form->field($model, 'phone')-> widget(\yii\widgets\MaskedInput::className(), [
													'mask' => '+7-999-9999999',
													'options' => ['class'=>'form-control', 'placeholder'=>$model->getAttributeLabel('phone')]
											])->label(false)?>
		<?= $form->field($model, 'password')->passwordInput(['placeholder'=>$model->getAttributeLabel('password')])->label(false)?>

		<center><?= $form->field($model, 'agreement')->checkBoxList([Yii::t('app', 'Ознакомлен с <a href="#">правилами</a>')])->label(false); ?></center>

		<center><input class="btn btn-success" type="submit" value="<?=Yii::t('app', 'Регистрация')?>"/></center>

		<?php ActiveForm::end(); ?>
	</div>

</div>
