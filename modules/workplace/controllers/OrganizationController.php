<?php

namespace app\modules\workplace\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use kujiorpamm\cropit\widgets\CropitWidget;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

use app\models\Files;
use app\modules\application\models\Application;
use app\modules\application\models\ApplicationTypes;
use app\modules\application\models\ApplicationAssignment;
use app\modules\application\models\ApplicationCommends;
use app\modules\application\models\ApplicationFiles;
use app\modules\organization\models\Organization;
use app\modules\organization\models\OrganizationProjects;
use app\modules\organization\models\OrganizationReports;


class OrganizationController extends Controller
{
    public $layout = "@workplace/views/layouts/main";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['organization'],
                    ],
                    [
                    	'actions' => ['projects', 'new-project'],
                    	'allow' => true,
                    	'roles' => ['organization', 'moderator', 'admin']
                    ]

                ],
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => ['class' => 'yii\web\ErrorAction'],
        ];
    }


    public function actionIndex() {

		/*Если пользователь контролёр - то у него
    	другой тип поведения на рабочем столе*/
		if(Yii::$app->user->can('controller')) {
			return $this->index_for_controller();
		} else {
			return $this->index_for_default();
		}


	}

	/*
	*	Контроллирующая организация видит заявки только в статусах
	*	- в работе
	*	- в заморозке
	*	Так же, заявки фильтруются по настройкам видимости
	*/
	public function index_for_controller() {

		$cookies = Yii::$app->response->cookies;
		if($post = Yii::$app->request->post()) {
			$cookies->add(new \yii\web\Cookie([
			    'name' => 'view-settings',
			    'value' => $post['types'],
			    'expire' => time() + 86400 * 30,
			]));

		}

		if($cookies->get('view-settings')) $types = $cookies->get('view-settings')->value;

		// Если указаны настройки видимости
		if($types) {
			$active = Application::find()->where(['IN', 'status', [2, 0]])->orderBy(['date_created'=>SORT_DESC])
				->andWhere(['IN', 'application_type', $types])->all(); // Статусы На проверке и В работе
			$active_count = Application::find()->where(['IN', 'status', [2, 0]])->orderBy(['date_created'=>SORT_DESC])
				->andWhere(['IN', 'application_type', $types])->count();
			$work_count = Application::find()->where(['IN', 'status', [2]])->orderBy(['date_created'=>SORT_DESC])
				->andWhere(['IN', 'application_type', $types])->count();
			$frozen_count = Application::find()->where(['IN', 'status', [0]])->orderBy(['date_created'=>SORT_DESC])
				->andWhere(['IN', 'application_type', $types])->count();
			$solved_count = Application::find()->where(['IN', 'status', [3]])->orderBy(['date_created'=>SORT_DESC])
				->andWhere(['IN', 'application_type', $types])->count();
		}
		// Если нет, то отображать все
		else {
			$active = Application::find()->where(['IN', 'status', [2, 0]])->orderBy(['date_created'=>SORT_DESC])->all(); // Статусы На проверке и В работе
			$active_count = Application::find()->where(['IN', 'status', [2, 0]])->orderBy(['date_created'=>SORT_DESC])->count();
			$work_count = Application::find()->where(['IN', 'status', [2]])->orderBy(['date_created'=>SORT_DESC])->count();
			$frozen_count = Application::find()->where(['IN', 'status', [0]])->orderBy(['date_created'=>SORT_DESC])->count();
			$solved_count = Application::find()->where(['IN', 'status', [3]])->orderBy(['date_created'=>SORT_DESC])->count();
		}

		$types = ApplicationTypes::getMainTypesAsArray();
		foreach($types as $key=>$val) {
			$types[$val] = ApplicationTypes::getSubTypesAsArray($key);
			unset($types[$key]);
		}

		return $this->render('@workplace/views/moderator/index_controller', [
			'active'=>$active,
			'active_count'=>$active_count,
			'solved_count'=>$solved_count,
			'new_count'=>$new_count,
			'work_count'=>$work_count,
			'frozen_count'=>$frozen_count,
			'types' => $types
		]);

	}

	/*
	*	Организация - исполнитель, заявки адресованные
	*	конкретной организации
	*/
	public function index_for_default() {
		$active = Application::find()
				->innerJoin('application_assignment', 'application.id = application_assignment.application_id')
				->where(['IN', 'status', [1, 2, 0]])->orderBy(['date_created'=>SORT_DESC])
				->andWhere(['=', 'application_assignment.organization_id', Yii::$app->user->identity->organization->id])
				->all(); // Статусы На проверке и В работе
		$active_count = Application::find()
				->innerJoin('application_assignment', 'application.id = application_assignment.application_id')
				->where(['IN', 'status', [1, 2, 0]])
				->andWhere(['=', 'application_assignment.organization_id', Yii::$app->user->identity->organization->id])
				->count();
		$solved_count = Application::find()
				->innerJoin('application_assignment', 'application.id = application_assignment.application_id')
				->where(['=', 'status', 3])
				->andWhere(['=', 'application_assignment.organization_id', Yii::$app->user->identity->organization->id])
				->count();
		$new_count = Application::find()
				->innerJoin('application_assignment', 'application.id = application_assignment.application_id')
				->where(['IN', 'status', [1]])
				->andWhere(['=', 'application_assignment.organization_id', Yii::$app->user->identity->organization->id])
				->count();
		$work_count = Application::find()
				->innerJoin('application_assignment', 'application.id = application_assignment.application_id')
				->where(['IN', 'status', [2]])->orderBy(['date_created'=>SORT_DESC])
				->andWhere(['=', 'application_assignment.organization_id', Yii::$app->user->identity->organization->id])
				->count();
		$frozen_count = Application::find()
				->innerJoin('application_assignment', 'application.id = application_assignment.application_id')
				->where(['IN', 'status', [0]])
				->andWhere(['=', 'application_assignment.organization_id', Yii::$app->user->identity->organization->id])
				->count();
		$total_count = Application::find()
				->innerJoin('application_assignment', 'application.id = application_assignment.application_id')
				->andWhere(['=', 'application_assignment.organization_id', Yii::$app->user->identity->organization->id])
				->count();

		return $this->render('@workplace/views/moderator/index', [
			'active'=>$active,
			'active_count'=>$active_count,
			'solved_count'=>$solved_count,
			'new_count'=>$new_count,
			'work_count'=>$work_count,
			'frozen_count'=>$frozen_count,
			'total_count'=>$total_count,
		]);
	}

	/*
	*	Просмотр заявки доступен организациям, только
	*	в статусе
	*	- в работе
	*	- в заморозке
	*	- выполнена
	*	- удалена
	*/
	public function actionView($id) {
    	$this->layout = "@workplace/views/layouts/main";

		$model = Application::findOne(['id'=>$id]);

    	if($model->status == 1) return $this->render('@workplace/views/moderator/waiting');
		$model_assignment = new ApplicationAssignment;
		$commend = new ApplicationCommends;
		$commend->application_id = $model->id;
		$model_assignment->application_id = $model->id;
		$model_assignment->application_end = $model->date_plane_end;
		$executors = Organization::find()->select(['name'])->indexBy('id')->column();
		return $this->render('@workplace/views/moderator/view', ['model'=>$model, 'assignment'=>$model_assignment, 'executors'=>$executors, 'commend'=>$commend]);
	}

	/*
	*	Архив заявок для организации.
	*	Если пользователь - контроллер, то
	*	отображать полный архив заявок, без фильтров
	*/
	public function actionArchive() {
		$this->layout = "@workplace/views/layouts/main";

		if($_POST['search-word']) $search = $_POST['search-word'];

		if(Yii::$app->user->can('controller')) {
			$query = Application::find();
		} else {
			//Только те заявки, что создавала моя организация, либо, где я принимал участие
			$user_organization_id = Yii::$app->user->identity->organization->id;
			$colleagues = Yii::$app->user->identity->colleaguesArray;
			$query = Application::find()->leftJoin('application_assignment', 'application_assignment.application_id = application.id')
										->where(['=', 'application_assignment.organization_id', $user_organization_id])
										->orWhere(['IN', 'application.author', [2]]);
		}

		if($search) $query->andWhere(['like', 'application.description', "{$search}"]);

		$dp = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
			],
			'sort' => [
				'defaultOrder' => [
					'date_end' => SORT_DESC,
					'date_created' => SORT_DESC,
					'date_created_human' => SORT_DESC,
				],
				'attributes' => [
					'date_end' => [
						'desc' => ['date_end'=>SORT_DESC],
						'asc' => ['date_end'=>SORT_ASC],
					],
					'date_created_human' => [
						'desc' => ['date_created'=>SORT_DESC],
						'asc' => ['date_created'=>SORT_ASC],
					],
					'statusName' => [
						'desc' => ['status'=>SORT_DESC],
						'asc' => ['status'=>SORT_ASC],
					]
				]
			]
		]);
		return $this->render('@workplace/views/moderator/archive', ['dp'=>$dp, 'search'=>$search]);
	}

	public function actionNewApplication() {
		$model = new Application();

		$model->date_created = date('d.m.Y H:i:s');
		$model->date_plane_end = date('d.m.Y H:i:s', strtotime(Yii::$app->params['application']['days_to_add']));
		$model->author = Yii::$app->user->id;
		$model->active = 1;
		$model->status = 1;

		if($model->load(Yii::$app->request->post())) {
			if($model->save()) {

				if($model->image_load) {
					$filename = "images/applications/" . $model->id . ".png";
					CropitWidget::saveAs($model->image_load, $filename);
					$file = new ApplicationFiles;
					$file->type = 1;
					$file->ext = 'png';
					$file->src = "/".$filename;
					$file->application_id = $model->id;
					$file->save();
				}

				return $this->redirect("/workplace");
			}
		}

		$category_items = ApplicationTypes::getMainTypesAsArray();
		return $this->render('@workplace/views/moderator/new-application', ['model'=>$model, 'category_items'=>$category_items]);
	}


	/*
	*
	*	Список проектов организации
	*
	*/
    public function actionProjects() {

    	$query = OrganizationProjects::find()->where(['organization_id'=>Yii::$app->user->identity->organization->id]);

    	$dp = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 10,
			],
			'sort' => [
				'defaultOrder' => [
					'date_end' => SORT_DESC
				]
			]
		]);

		return $this->render('@workplace/views/default/projects', ['dp'=>$dp]);
	}

	public function actionRemoveProject() {

	}

	/*
	*
	*	Добавить новый проект от организации
	*	Добавляется самой организацией
	*
	*/
    public function actionNewProject() {
		$model = new OrganizationProjects();
		$model->author_id = Yii::$app->user->identity->id;
		$model->organization_id = Yii::$app->user->identity->organization->id;
		if($model->load(Yii::$app->request->post())) {
			$model->save();
			return $this->redirect('/workplace/organization/projects');
		}

		return $this->render('@workplace/views/moderator/new-project', ['model'=>$model]);
	}

  public function actionReports() {
    return $this->render('@workplace/views/organization/reports', ['dp'=>OrganizationReports::getOrganizationDP()]);
  }

  public function actionNewReport() {
    $model = new OrganizationReports();
    $model->author_id = Yii::$app->user->identity->id;
    $model->organization_id = Yii::$app->user->identity->organization->id;
    $model->date = date('Y-m-d');

    if($model->load(Yii::$app->request->post())) {
      if($model->save()) {
        return $this->redirect('/workplace/organization/update-report?id='.$model->id);
      } else {
        var_dump($model->getErrors());
        exit;
      }

		}
    return $this->render('@workplace/views/organization/new-report', ['model'=>$model]);
  }

  // Редактирование отчета
  public function actionUpdateReport($id) {
    $file = new Files;
    $report = OrganizationReports::findOne(['id'=>$id]);

    if(!$report) throw new \Exception("Ошибка", 1);
    if($report->organization_id !== Yii::$app->user->identity->organization->id) throw new \Exception("Ошибка 2", 1);

    $file->scenario = 'create';
    $file->table_name = OrganizationReports::tableName();
    $file->table_id = $report->id;

    // Сохранение отчета
    if($report->load(Yii::$app->request->post())) {
      if($report->save()) {
        return $this->refresh();
      } else {
        throw new \Exception("Ошибка при сохранении", 1);
      }
		}

    //Сохранение файлов
    if($file->load(Yii::$app->request->post())) {
      $file->file = UploadedFile::getInstance($file, 'file');
      $file->prepare();

      if($file->validate() && $file->save()) {
        return $this->refresh();
      } else {
        var_dump($file->getErrors()); exit;
      }
    }

    return $this->render('@workplace/views/organization/update-report', ['file'=>$file, 'report'=>$report]);
  }

  public function actionDeleteFile($id) {
    $file = Files::findOne(['id'=>$id]);
    $report = OrganizationReports::findOne(['id'=>$file->table_id]);

    if(!$file) throw new \Exception("Ошибка", 1);
    if($report->organization_id !== Yii::$app->user->identity->organization->id) throw new \Exception("Ошибка 2", 1);

    try {
      unlink(substr($file->path, 1));
      $file->delete();
    } catch (\Exception $e) {
      throw new \Exception("Ошибка при удалении файла: $e", 1);
    }

    return $this->redirect(Yii::$app->request->referrer);

  }

  // public function actionUpdateReport($id) {
  //   $model = OrganizationReports::findOne(['id'=>$id]);
  //   if(!$model) throw new \Exception("Отчет не найден", 1);
  //   if($model->load(Yii::$app->request->post())) {
  //     if($model->save()) {
  //       return $this->redirect('/workplace/organization/reports');
  //     } else {
  //       var_dump($model->getErrors());
  //       exit;
  //     }
  //
	// 	}
  //   return $this->render('@workplace/views/organization/edit-report', ['model'=>$model]);
  // }

  public function actionDeleteReport($id) {
    OrganizationReports::findOne(['id'=>$id])->delete();
		return $this->redirect(Yii::$app->request->referrer);
  }

}
