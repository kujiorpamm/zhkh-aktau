<?php

?>

<div class="project-item item">
	<div class="row">
		<div class="col-sm-6">
			<?=$model->name_ru?>
		</div>
		<div class="col-sm-4">
			<span class="moment-date-base"><?=$model->date_1?></span> - <span class="moment-date-base"><?=$model->date_2?></span>
		</div>
		<div class="col-sm-2">
			<a class="confirm-link" href="/organization/default/remove-project?id=<?=$model->id?>"><i class="glyphicon glyphicon-remove"> </i></a>
		</div>
	</div>	
	<div class="descr"><?=$model->descr_ru?></div>
</div>