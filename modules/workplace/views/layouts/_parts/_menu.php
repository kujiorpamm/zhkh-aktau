<?php
	use yii\helpers\Html;
?>

<div class="heading">
	<div class="top">
		<div class="inline">
			<div class="logo">
				<a href="/"><img class="img img-responsive" src="/images/style/logo-aqutau.png" title="<?=Yii::t('app', 'На главную')?>"/></a>
			</div>
			<div class="title">
				<?=Yii::t('app', 'Мобильное<br/>Управление<br/>г.Актау')?>
			</div>
		</div>
	</div>
	<div class="bottom">
		<i class="glyphicon glyphicon-user"></i>
		<?=Yii::$app->user->identity->shortName?> (<?=Yii::$app->user->identity->organization->name?>)
	</div>
</div>

<div class="links">

	<a href="/workplace"><?=Yii::t('app', 'Рабочий стол')?></a>
	<a href="/archive"><?=Yii::t('app', 'Архив заявок')?></a>
	<a href="/workplace/organization/projects"><?=Yii::t('app', 'Архив проектов')?></a>

	<?php if(Yii::$app->user->can('organization')): ?>
		<a href="/workplace/organization/reports"><?=Yii::t('app', 'Отчеты')?></a>
	<?php endif; ?>

	<?php if(Yii::$app->user->can('maek')):?> <a href="/dashboard/admin/stats"><?=Yii::t('app', 'Показания МАЭК')?></a> <?php endif;?>

	<!--Статистика-->
	<?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('moderator') || Yii::$app->user->can('controller')):?>
		<div class="separator"><?=Yii::t('app', 'Статистика')?>
			<a href="/statistics"><?=Yii::t('app', 'Общая')?></a>
			<a href="/statistics/default/for-excell"><?=Yii::t('app', 'Отчетная')?></a>
			<a href="/statistics/uncompleted"><?=Yii::t('app', 'Неисполненные заявки')?></a>
			<!--<a href="/statistics/default/by-type"><?=Yii::t('app', 'По типам')?></a>-->
		</div>
	<?php endif;?>
	<!--КОНЕЦ СТАТИСТИКА-->

	<!--АДМИН-->
	<?php if(Yii::$app->user->can('admin')):?>
		<div class="separator"><?=Yii::t('app', 'Администрирование')?>
			<a href="/user/admin"><?=Yii::t('app', 'Пользователи')?></a>
			<a href="/organization/admin"><?=Yii::t('app', 'Организации')?></a>
			<a href="/organization/admin/addresses"><?=Yii::t('app', 'Адреса')?></a>
			<a href="/application/admin/application-types"><?=Yii::t('app', 'Типы проблем')?></a>
			<a href="/dashboard/admin/stats"><?=Yii::t('app', 'Показания МАЭК')?></a>
		</div>
	<?php endif;?>
	<!--КОНЕЦ АДМИН-->


	<!--МОДЕРАТОР-->
		<?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('moderator')):?>
			<?= app\modules\notification\widgets\AdminLinkWidget::widget()?>
		<?php endif;?>
	<!--КОНЕЦ МОДЕРАТОР-->

	<a class="dd-menu" href="/" title="<?=Yii::t('app', 'На главную')?>"><?=Yii::t('app', 'На главную')?></a>
	<?php
		if(!Yii::$app->user->isGuest) {
			echo Html::beginForm(['/site/logout'], 'post');
			echo Html::submitButton(
	            Yii::t('app', 'Выход'),
	            ['class' => 'btn btn-link btn-block logout']
	        );
	        echo Html::endForm();
		}
	?>

</div>
