<?php

use yii\bootstrap\Modal;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

Modal::begin([
	'id' => 'settings',
   	'header' => Yii::t('app', 'Укажите, какие типы заявок отображать:'),
   	//'clientOptions' => ['show'=>true] 
]);

?>

<?php $form = ActiveForm::begin(); ?>
<div class="cnt">
<?php
	$cookies = Yii::$app->response->cookies;
	if($cookies->get('view-settings')) $settings = $cookies->get('view-settings')->value;
	//print_r($settings);
	foreach($types as $key => $val) {
		echo "<div class='box-heading'>{$key}</div>";
		echo Html::checkboxList('types', $settings, $val, ['item'=>function($index, $label, $name, $checked, $value){
			//if(in_array($value, $settings)) return "1";
			//echo $value . " " . $checked . "<br/>";
			return "<div class='checkbox'>
				<label>
					<input name='$name' type='checkbox' value='{$value}' ". (($checked)?'checked="1"':'') .">
					{$label}
				</label>
			</div>";	
		}]);
		//echo $val . " " . in_array($val, $settings) . "<br/>";
	}
?>
</div>

<button type="submit" class="btn btn-success btn-block"><?=Yii::t('app', 'Сохранить')?></button>

<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>
