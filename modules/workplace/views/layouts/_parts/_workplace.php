<?php
	
	use app\modules\application\controllers\ApplicationController;
	
	$new_app_href = (Yii::$app->user->can("moderator") || Yii::$app->user->can("admin")) ? "/workplace/moderator/new-application" : "/workplace/organization/new-application";
	
	
	
?>


<div class="workplace">
	
	<div class="heading">
		<div class="clearfix">
			<div class="pull-left">
				<h3><?=Yii::t('app', 'Рабочий стол')?></h3>
				<div><?=Yii::t('app', 'Список заявок, адресованных моей организации')?></div>
			</div>
			<div class="pull-right">
				<div class="stats">
					<div class="item">
						<div class="value"><?=$active_count?></div>
						<div class="text"><?=Yii::t('app', 'Активно<br/>заявок')?></div>
					</div>
					<?php if(Yii::$app->user->identity->getRole() !== 'controller'): ?>
					<div class="item">
						<div class="value"><?=$total_count?></div>
						<div class="text"><?=Yii::t('app', 'Получено<br/>заявок')?></div>
					</div>
					<?php endif; ?>					
					<div class="item">
						<div class="value"><?=$solved_count?></div>
						<div class="text"><?=Yii::t('app', 'Решено<br/>заявок')?></div>
					</div>
				</div>
			</div>
		</div>
		<hr/>
		<div class="controller">
			<div class="clearfix">
				<div class="pull-left">
					<div class="btn-group">
						<button class="btn btn-link as-link active act" data-attr="status-1,status-2,status-3" ><?=Yii::t('app', 'Все ({0})', [$active_count])?></button>
						<?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('moderator')) : ?>
						<button class="btn btn-link as-link act" data-attr="status-1"><?=Yii::t('app', 'Только новые ({0})', [$new_count])?></button>
						<?php endif; ?>
						<button class="btn btn-link as-link act" data-attr="status-2"><?=Yii::t('app', 'Только в работе ({0})', [$work_count])?></button>
						<button class="btn btn-link as-link act" data-attr="status-0"><?=Yii::t('app', 'Замороженные ({0})', [$frozen_count])?></button>
					</div>	
				</div>
				<div class="pull-right">
					<div class="app-btns">
						<a href="<?=$new_app_href?>" class="btn btn-sm btn-default btn-success"><?=Yii::t('app', 'Добавить заявку')?></a>
						<a href="/workplace/organization/new-project" class="btn btn-default  btn-sm btn-success"><?=Yii::t('app', 'Добавить уведомление')?></a>
						<?php if(Yii::$app->user->can('controller')): ?>
						<button class="btn btn-default btn-sm" onclick="$('#settings').modal('show');" title="<?=Yii::t('app', 'Настройки')?>"><i class="glyphicon glyphicon-eye-open"></i></button>
						<?php endif; ?>
						
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
	<div class="application-container">
		<div class="application-list">
			<div class="content">
				<div class="application-group">
					<div class="content">
						<?php
							
							foreach($active as $application) {
								echo ApplicationController::render_application_adm($application);
							}
							
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
</div>

<?php
	if(Yii::$app->user->can('controller')) {
		echo $this->render('_settings', ['types' => $types]);
	}
?>