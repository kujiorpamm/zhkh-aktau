<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\modules\workplace\assets\WorkplaceAsset;

//AppAsset::register($this);
WorkplaceAsset::register($this);

$this->title = Yii::$app->params['siteName'];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?=$this->render('_parts/_header')?>

<div class="wrap wrap-admin">
    

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        
        <div class="row">
        	<div class="col-sm-3">
        		
        		<ul>
					<li><a href="/application/admin/application-types">Типы проблем</a></li>
					<li><a href="/user/admin">Пользователи</a></li>
					<li><a href="/organization/admin">Организации</a></li>
					<li><a href="/organization/admin/addresses">Адреса</a></li>					
					<li><a href="/dashboard/admin/stats">Показания МАЭК</a></li>
				</ul>
        		
        	</div>
        	
        	<div class="col-sm-9">
        		<?= $content ?>
        	</div>
        </div>
        
    </div>
</div>
<?=$this->render('@app/views/layouts/_parts/_popup')?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
