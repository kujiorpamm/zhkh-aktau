<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use kartik\datetime\DateTimePicker;
	
	use kujiorpamm\cropit\widgets\CropitWidget;
	use app\modules\application\assets\ApplicationCreateAsset;
	
	ApplicationCreateAsset::register($this);
	$canAdmin = Yii::$app->user->identity->canAdmin();
?>

<?php $form = ActiveForm::begin(['id'=>'app_form']); ?>

<div class="row">
	<!--ЛЕВАЯ КОЛОНКА-->
	<div class="col-md-6 col-sm-8 col-xs-12">		
		<div class="form-splitter"><?=Yii::t('app', 'Общие данные')?></div>
		<div>
			<!--Категория-->
			<div class="form-group">
				<!--<label><?=Yii::t('app', 'Категория')?></label>-->
				<?=$form->field($model, 'main_category')->dropDownList($category_items, ['prompt'=>'Выберите категорию', 'id'=>'app_main_cat'])->label(false)?>
			</div>
			
			<!--Подкатегория-->
			<div>
				<?=$form->field($model, 'application_type')->dropDownList(($subcategory_items)?$subcategory_items: [], ['prompt'=>'Выберите подкатегорию'])->label(false)?>
			</div>
			
			<!--Описание-->
			<div class="form-group">
				<!--<label><?=Yii::t('app', 'Текст заявки')?></label>-->
				<?=$form->field($model, 'description')->textArea(['placeholder'=>Yii::t('app', 'Текст заявки')])->label(false)?>
			</div>
			
			<!--Даты-->
			<?php if($canAdmin): ?>
			<div class="row">
				<div class="col-sm-6">
					<!--Дата создания-->
					<div class="form-group">
						<label><?=Yii::t('app', 'Дата <br/>создания')?> <span class="note glyphicon glyphicon-question-sign"><div>Дата создания заявки (по умолчанию - сегодня)</div></span></label>
						<?=$form->field($model, 'date_created')->widget(DateTimePicker::classname(), [
						    'options' => ['placeholder' => Yii::t('app', 'Дата создания')],
						    'pluginOptions' => [
								'todayHighlight' => true,
								'todayBtn' => true,
								'format' => 'dd.mm.yyyy hh:ii:ss',
								'autoclose' => true,
							]
						])->label(false)?>
					</div>
				</div>
				<div class="col-sm-6">
					<!--Дата планируемого завершения-->
					<div class="form-group">
						<label><?=Yii::t('app', 'Дата  <br/>планируемого завершения')?> <span class="note glyphicon glyphicon-question-sign"><div>Дата планируемого завершения заявки (по умолчанию - плюс один день)</div></span> </label>
						<?=$form->field($model, 'date_plane_end')->widget(DateTimePicker::classname(), [
						    'options' => ['placeholder' => Yii::t('app', 'Дата планируемого завершения')],
						    'pluginOptions' => [
								'todayHighlight' => true,
								'todayBtn' => true,
								'format' => 'dd.mm.yyyy hh:ii:ss',
								'autoclose' => true,
							]
						])->label(false)?>
					</div>
				</div>
			</div>
			<?php endif;?>
			
			<!--Важность-->
			<?php if($canAdmin): ?>
			<div class="form-group">
				<label><?=Yii::t('app', 'Важность')?></label>
				<?=$form->field($model, 'seriosness')->dropDownList(Yii::$app->params['application']['seriosness'])->label(false)?>
			</div>
			<?php endif;?>
		</div>
		<?php if(!$canAdmin): ?>
		<div class="form-splitter"><?=Yii::t('app', 'Фотография')?> <span class="note glyphicon glyphicon-question-sign"><div>Фотография 800x600 </div></span></div>
		
		<?php
			if($is_editing) echo "<img src='{$model->imageBefore}' class='img img-responsive'/>";
		?>	
		
		<div class="image-loader">
			<?= $form->field($model, 'image_load')->widget(\kujiorpamm\cropit\widgets\CropitWidget::className(), [
				'pluginOptions' => [
					'width' => 400,
					'height' => 300,
					'smallImage' => 'stretch',
					'exportZoom' => 2
			]])->label(false);?>
		</div>
		<?php endif;?>
	</div>
	<!--КОНЕЦ ЛЕВАЯ КОЛОНКА-->
	
	<!--ПРАВАЯ КОЛОНКА-->
	<div class="col-md-6 col-sm-4 col-xs-12">
		
		<div class="form-splitter"><label for="Application[coords]"><?=Yii::t('app', 'Координаты')?></label></div>
		<div class="same-height">
			<!--Метка на карте-->
			<div>
				<?=$form->field($model, 'coords')->hiddenInput()->label(false)?>
			</div>
			
			<div class="yandex-map" id="ymap">
				
			</div>
		</div>
		
	</div>
	<!--КОНЕЦ ПРАВАЯ КОЛОНКА-->
	
	
</div>
<?php if($canAdmin): ?>
<div class="row">
	<div class="col-md-6 col-sm-8 col-xs-12">
		
		<div class="form-splitter"><?=Yii::t('app', 'Данные заявителя')?> <span class="note glyphicon glyphicon-question-sign"><div>Данные заявителя - те, заявки, которые пришли по телефону. Обязательно указать эти данные. В другому случае - пропустить. <br/><br/> Телефон заявителя автоматичесски вводится в формате 99-99-99 или 999-9999999</div></span></div>
			
		<!--Заявка с телефона: Имя заявителя-->
		<div class="form-group">
			<!--<label><?=Yii::t('app', 'Имя заявителя')?></label>-->
			<?=$form->field($model, 'applicant_name')->textInput(['placeholder'=>Yii::t('app', 'Имя заявителя')])->label(false)?>
		</div>
		<!--Заявка с телефона: Телефон заявителя-->
		<div class="form-group">
			
			<?=$form->field($model, 'applicant_phone')-> widget(\yii\widgets\MaskedInput::className(), [
								    'mask' => ['99-99-99','999-9999999'],
								    'options' => ['class'=>'form-control', 'placeholder'=>'Номер телефона'],
								    'clientOptions'=>['clearIncomplete'=>true]
								])->label(false)?>
			
			
		</div>
		<!--Заявка с телефона: Телефон заявителя-->
		<div class="form-group">
			<!--<label><?=Yii::t('app', 'Адрес заявителя')?></label>-->
			<?=$form->field($model, 'applicant_addr')->textArea(['placeholder'=>Yii::t('app', 'Адрес заявителя')])->label(false)?>
		</div>
		
	</div>
	<div class="col-md-6 col-sm-4 col-xs-12">
		<div class="form-splitter"><?=Yii::t('app', 'Фотография')?> <span class="note glyphicon glyphicon-question-sign"><div>Фотография 800x600 </div></span></div>
		
		<?php
			if($is_editing) echo "<img src='{$model->imageBefore}' class='img img-responsive'/>";
		?>	
		
		<div class="image-loader">
			<?= $form->field($model, 'image_load')->widget(\kujiorpamm\cropit\widgets\CropitWidget::className(), [
				'pluginOptions' => [
					'width' => 400,
					'height' => 300,
					'smallImage' => 'stretch',
					'exportZoom' => 2
			]])->label(false);?>
		</div>
	</div>
</div>
<?php endif;?>

<div class="clearfix">
	<div class="pull-right">
		<button type="submit" class="btn btn-lg btn-success"> <?=Yii::t('app', ($is_editing)?'Обновить' : 'Добавить заявку')?> </button>
	</div>
</div>

<?php ActiveForm::end(); ?>