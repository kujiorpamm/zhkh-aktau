<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Modal;
	use yii\bootstrap\ActiveForm;
	use kartik\date\DatePicker;
	use kartik\datetime\DateTimePicker;
	use kujiorpamm\cropit\widgets\CropitWidget;
?>

<?php	
	Modal::begin([
		'id' => 'm_form_status',
	    'header' => Yii::t('app', 'Изменение статуса'),
	    'size' => 'modal-md',
	    'options' => [
	    	'tabindex' => false
	    ]/*,
	    'clientOptions'=> [
	    	'show' => true
	    ]*/
	]);
?>
	
	<?php $form = ActiveForm::begin(['action'=>'/workplace/moderator/change-status']); ?>
		
		<?=$form->field($model, 'id')->hiddenInput()->label(false)?>
		<div class="form-group">
			<?=$form->field($model, 'status')->dropDownList(Yii::$app->params['application']['status'], ['prompt'=>Yii::t('app', 'Выберите статус')])->label(false)?>
		</div>
		
		<!--Дата планируемого завершения-->
		<div class="form-group">
			<label><?=Yii::t('app', 'Дата планируемого завершения')?> <span class="note glyphicon glyphicon-question-sign"><div>Дата планируемого завершения заявки, при заморозке обязательно указать дату разморозки!</div></span> </label>
			<?=$form->field($model, 'date_plane_end')->widget(DateTimePicker::classname(), [
			    'options' => ['placeholder' => Yii::t('app', 'Дата планируемого завершения')],
			    'pluginOptions' => [
					'todayHighlight' => true,
					'todayBtn' => true,
					'format' => 'dd.mm.yyyy hh:ii:ss',
					'autoclose' => true,
				]
			])->label(false)?>
		</div>
		
		<!--Изображение -решение -->
		<div class="image-loader form-group">
			<label><?=Yii::t('app', 'Изображение')?> <span class="note glyphicon glyphicon-question-sign"><div>Обязательно укажите изображение выполненной заявки, если заявка выполнена. Скачайте его из комментария исполнителя.</div></span> </label>
			<?= $form->field($model, 'image_load')->widget(\kujiorpamm\cropit\widgets\CropitWidget::className(), [
				'pluginOptions' => [
					'width' => 400,
					'height' => 300,
					'smallImage' => 'stretch',
					'exportZoom' => 2
			]])->label(false);?>
		</div>
		
		
		<button type="submit" class="btn btn-success btn-block"> <?=Yii::t('app', 'Изменить')?> </button>
		
	<?php ActiveForm::end(); ?>
	
	
<?php
	Modal::end();
?>

<?=$this->registerJs("

	

")?>