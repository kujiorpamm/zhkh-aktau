<?php
	use yii\helpers\Html;
	use kartik\grid\GridView;
?>

<div class="workplace archive">
	
	<div class="heading">
		<h3><?=Yii::t('app', 'Архив')?></h3>
		<div><?=Yii::t('app', 'Заявки созданные вашей организацией и заявки, где вы принимали участие')?></div>
		
	</div>
	
	<div class="application-container">
		<?=GridView::widget([
		    'dataProvider'=> $dp,
		    'columns' => [
		    		'id',
		    		['class'=>'\kartik\grid\DataColumn', 'attribute'=>'authorName', 'hidden'=>true],
		    		'fulltype',
		    		'statusName',
		    		'date_created_human',
		    		['class'=>'\kartik\grid\DataColumn', 'attribute'=>'date_plane_end', 'hidden'=>true],
		    		'date_end',
		    		['class'=>'\kartik\grid\DataColumn', 'attribute'=>'organizationNames', 'hidden'=>false],
		    		['class'=>'\kartik\grid\DataColumn', 'attribute'=>'link', 'hidden'=>false, 'format'=>'html', 'hAlign'=>'center'],
		    	],
		    'responsive'=>true,
		    'hover'=>true,
		    'striped'=>false,
		    'pjax'=>false,
		    'toolbar'=> false,
		    'export'=>[
		        'fontAwesome'=>false,
		        'options' => [
		        	'class' => 'btn-export',
		        ]
		    ],
		    'exportConfig'=> [
				GridView::EXCEL => true
			],
		    'panel'=>[
	        	'type'=>GridView::TYPE_DEFAULT,
	        	'footer' => false
		    ],
		    'panelHeadingTemplate' => $this->render('_parts/_form_search', ['dp'=>$dp, 'search'=>$search]),
		]);?>
	</div>
	
	
</div>