<?php
	$this->title = $model->id;
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рабочий стол'), 'url' => ['/workplace/moderator']];
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Заявка №{0}', [$model->id]), 'url' => ['/workplace/moderator/view?id='.$model->id]];
	$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>

<script>
	var app_coords = [<?=$model->coords?>];
</script>

<div class="workplace archive">
	
	<div class="heading">
		<h3><?=Yii::t('app', 'Редактирование завки № {0}', [$model->id])?></h3>
		
	</div>
	
	<div class="application-container">
		<?=$this->render("_parts/_app_form", ['model'=>$model, 
										'category_items'=>$category_items, 
										'subcategory_items'=>$subcategory_items, 
										'is_editing' => true])?>
	</div>
	
	
</div>
