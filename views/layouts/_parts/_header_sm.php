<?php	
use yii\helpers\Html;
?>

<div class="wrap header-main">
	<div class="container no-padding">
		
		<div class="clearfix">
			<div class="pull-left">					
				<div class="inline">
					<a href="/">
						<div class="logo">
							<img src="/images/style/logo-aqutau.png"/>
						</div>
					</a>
					
					<div class="logo-text">
						<h1 class="text1"><?=Yii::t('app', 'МОБИЛЬНОЕ УПРАВЛЕНИЕ')?></h1>
						<h1 class="text2"><?=Yii::t('app', 'ГОРОДА АКТАУ')?></h1>
					</div>						
				</div>
			</div>
			
			<div class="pull-right hidden-xs">
				<div class="inline">
					<div class="links">
						<a href="/report"><?=Yii::t('app', 'Сообщить о проблеме')?></a>
					</div>
					<div class="div">
						<div class="language">
							<div class="picker kz"><a href="/site/lang?lang=kk">ҚАЗ</a></div>
							<a href="/site/lang"><div class="switch <?=Yii::$app->language?>"></div></a>
							<div class="picker ru"><a href="/site/lang?lang=ru">РУС</a></div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		
		<!--<div class="undp"><img src="/images/style/logo-undp.png"/></div>-->
		
	</div>
</div>

<?=$this->render('_menu')?>