<?php	
use yii\helpers\Html;
?>

<div class="wrap menu-sm">
	<div class="container">
		<div class="menu-list">
			
			<!--МЕНЮ MD-->
			<div class="clearfix hidden-xs">
				<div class="pull-left">
					<a href="/event-map"><?=Yii::t('app', 'Карта событий')?></a>
					<a href="/organizations"><?=Yii::t('app', 'ОУОК')?></a>
					<!--<a href="#"><?=Yii::t('app', 'Результаты')?></a>-->
					<a href="/about"><?=Yii::t('app', 'О портале')?></a>
					<a href="/me"><?=Yii::t('app', 'Личный кабинет')?></a>
					
					<?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('moderator') || Yii::$app->user->can('organization')) : ?>
					<a href="/workplace"><?=Yii::t('app', 'Рабочий стол')?></a>
					<?php endif; ?>
					
					
				</div>
				
				<div class="pull-right inline">
					
					
					<!--<a id="menu-main" class="dd-menu" href="#" title="<?=Yii::t('app', 'Дополнительное меню')?>"><img src="/images/style/icon-menu.png"/></a>-->
					
					<div class="hover-light">
					<?php
						if(!Yii::$app->user->isGuest) {
							echo Html::beginForm(['/site/logout'], 'post');
							echo Html::submitButton(
			                    Html::a('<img src="/images/style/icon-logout.png" title="'.Yii::t('app', 'Выход').'"/>', '/login', ['class'=>'dd-menu']),
			                    ['class' => 'btn btn-link logout']
			                );
			                echo Html::endForm();
						} else {
							//echo Html::a('<img src="/images/style/icon-login.png" title="'.Yii::t('app', 'Вход в портал').'"/>', '/login', ['class'=>'dd-menu']);
							echo Html::a(Yii::t('app', 'Вход / регистрация'), '/login', ['class'=>'dd-menu']);
						}
					?>
					</div>
				</div>
				
			</div>
			
			<!--МЕНЮ SM-->
			<div class="clearfix visible-xs">
				<div class="pull-right inline">
					<a class="xs-menu-trigger" href="#" title="<?=Yii::t('app', 'Дополнительное меню')?>"><img src="/images/style/icon-menu.png"/></a>
				</div>
				
			</div>
		</div>		
	</div>
</div>

<div class="wrap menu-xs visible-xs">
	
	<div class="menu-icon">
		<a class="xs-menu-trigger" href="#" title="<?=Yii::t('app', 'Дополнительное меню')?>"><img src="/images/style/icon-menu-black.png"/></a>
		
		<div class="menu-list">
			<div class="item">  <a href="/report"> <img src="/images/style/menu-app.png"/> <?=Yii::t('app', 'Сообщить о проблеме')?></a></div>
			<div class="item"><a href="/event-map"> <img src="/images/style/menu-map.png"/> <?=Yii::t('app', 'Карта событий')?></a></div>
			<div class="item"><a href="/event-map"> <img src="/images/style/menu-calendar.png"/> <?=Yii::t('app', 'Планируемые работы')?></a></div>
			<div class="item"><a href="/news"> <img src="/images/style/menu-news.png"/> <?=Yii::t('app', 'Архив новостей')?></a></div>
			<div class="item"><a href="/organizations"> <img src="/images/style/menu-worker.png"/> <?=Yii::t('app', 'ОУОК')?></a></div>
			<div class="item"><a href="/about"> <img src="/images/style/menu-q.png"/> <?=Yii::t('app', 'О портале')?></a></div>
			<div class="delim"></div>
			
			<?php if(Yii::$app->user->isGuest): ?>
				<div class="item"><a href="/login"> <img src="/images/style/menu-login.png"/> <?=Yii::t('app', 'Вход в портал')?></a></div>
				<div class="item"><a href="/registration"> <img src="/images/style/menu-register.png"/> <?=Yii::t('app', 'Регистрация')?></a></div>
			<?php else : ?>
				<div class="item"><a href="/me"> <img src="/images/style/menu-user.png"/> <?=Yii::t('app', 'Личный кабинет')?></a></div>
			<?php endif; ?>
		</div>
		
	</div>
	
</div>