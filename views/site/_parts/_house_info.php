<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
?>


<div class="input-group inline">
	
	<?php
		echo \kartik\select2\Select2::widget([
		    'id' => 'address_id',
		    'name' => 'address_id',
		    'data' => $addresses,
		    'options' => [
		        'placeholder' => Yii::t('app', 'Начните вводить адрес'),
		        'required' => true
		    ],
		    
		    'pluginEvents' => [
		    	'change' => 'function() { os_event("change"); }',
		    ]
		]);
	?>
    
</div>

<div class="result" style="display: none;">
	<span><?=Yii::t('app', 'Ваша управляющая организация: <a href="#">КПА АРАК</a>')?></span> <br/>
	<img width="40" src="/images/style/loading.gif"/>
</div>