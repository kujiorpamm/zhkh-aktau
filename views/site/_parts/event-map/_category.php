<div class="category closed">
	<div class="name" data-type="category" data-id="<?=$model['id']?>">
		<div class="icon"><img src="<?=$model['icon']?>"/></div>
		<div class="title"><?=$model['name']?></div>
	</div>
	<div class="items">
	<?php
		foreach($model['items'] as $itm) {
			echo $this->render('_item', ['model'=>$itm]);
		}
	?>
	</div>
	
</div>