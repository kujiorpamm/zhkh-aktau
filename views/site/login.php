<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Вход в портал');
?>

<div class="page-index">
	
	<div class="wrap white">
		<div class="container">
			<div class="site-login">
			
			<h2><?=Yii::t('app', 'Вход в портал')?></h2>
			
		    <div class="row">
			    <div class="col-sm-6 col-sm-offset-3">
			    	<?php $form = ActiveForm::begin([
				        'id' => 'login-form'
				    ]); ?>

				        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder'=>'E-mail'])->label(false) ?>

				        <?= $form->field($model, 'password')->passwordInput(['placeholder'=>Yii::t('app', 'Пароль')])->label(false) ?>

				        <div class="form-group">
				           <?= Html::submitButton(Yii::t('app', 'Войти'), ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
				        </div>

				    <?php ActiveForm::end(); ?>
				    
				    <div class=""><a href="#"><?=Yii::t('app', 'Я забыл пароль')?></a></div>
				    <div class="register"><?=Yii::t('app', 'Еще не с нами? Присоединяйтесь, сделаем Актау чище вместе! <br/><a href="/registration">Регистрация</a>')?></div>
				    
			    </div>
		    </div>
		    
		</div>
		</div>
	</div>
</div>

