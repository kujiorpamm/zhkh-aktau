<?php
	use yii\bootstrap\ActiveForm;
	use kartik\date\DatePicker;
	
	app\assets\EventMapAsset::register($this);
	
	$this->registerJs("var _items = ".$json_projects, $this::POS_BEGIN);
?>

<div class="wrap white event-map">
	<div class="container">
		
		<div class="row">
			<div class="col-sm-4">
				
				<div class="map-switch">
					<a href="/event-map" class="btn custom btn-strainght"><?=Yii::t('app', 'Активные заявки')?></a>
					<a href="/project-map" class="btn custom btn-strainght active"><?=Yii::t('app', 'Планируемые работы')?></a>
				</div>
				
				<div class="project-list">
					
					<div class="item">
						<div class="heading"><?=Yii::t('app', 'Сейчас активны')?></div>
						<div class="content">
							
							<?php
								foreach($active as $act) {
									echo $this->render('_parts/_project_item', ['model'=>$act]);
								}
							?>
						</div>
					</div>
					
					<div class="item">
						<div class="heading"><?=Yii::t('app', 'Начнутся в этом месяце')?></div>
						<div class="content">
							<?php
								foreach($this_month as $act) {
									echo $this->render('_parts/_project_item', ['model'=>$act]);
								}
							?>
						</div>
					</div>
					
					<div class="item">
						<div class="heading"><?=Yii::t('app', 'Начнутся позже')?></div>
						<div class="content">
							<?php
								foreach($other as $act) {
									echo $this->render('_parts/_project_item', ['model'=>$act]);
								}
							?>
						</div>
					</div>
					
				</div>
				
				<div class="menu-container">
					<div class="menu">
						<?php
							foreach($categores as $cat) {
								//echo $this->render('_parts/event-map/_category', ['model'=>$cat]);
							}
						?>
						<div class="scrollbar"></div>
					</div>
				</div>	
			</div>
			<div class="col-sm-8">
				<div id="map" class="map"></div>
			</div>
		</div>
		
	</div>
	
</div>