/*
Counts.js - файл js-скриптов получения и обработки данных для контрольной панели 
Компания разработчик: infographics.kz
Использование настоящей библиотеки без разрешение автора не разрешается
*/
/* ======================= ПОЛУЧЕНИЕ ТЕКУЩЕЙ ДАТЫ ПОКАЗАНИЙ ПРИБОРОВ ================== */
if (document.getElementById('curr_date')) document.getElementById('data').innerHTML = document.getElementById('curr_date').value;

/* ======================= ПОЛУЧЕНИЕ ДАННЫХ ДЛЯ СЧЕТЧИКОВ ВОДЫ ================== */
document.getElementById('dev1').innerHTML = document.getElementById('dv-1').value;
document.getElementById('dev2').innerHTML = document.getElementById('dv-2').value;
document.getElementById('dev3-1').innerHTML = document.getElementById('dv-3').value;
document.getElementById('dev3-2').innerHTML = document.getElementById('dv-4').value;
document.getElementById('dev3-3').innerHTML = document.getElementById('dv-5').value;
document.getElementById('dev4-1').innerHTML = document.getElementById('dv-6').value;
document.getElementById('dev4-2').innerHTML = document.getElementById('dv-7').value;
document.getElementById('dev4-3').innerHTML = document.getElementById('dv-8').value;
document.getElementById('dev5-1').innerHTML = document.getElementById('dv-9').value;
document.getElementById('dev5-2').innerHTML = document.getElementById('dv-10').value;
document.getElementById('dev5-3').innerHTML = document.getElementById('dv-11').value;

document.getElementById('dev-1-1').innerHTML = document.getElementById('d-1').value;
document.getElementById('dev-1-2').innerHTML = document.getElementById('d-2').value;
document.getElementById('dev-1-3').innerHTML = document.getElementById('d-3').value;
document.getElementById('dev-2-1').innerHTML = document.getElementById('d-4').value;
document.getElementById('dev-2-2').innerHTML = document.getElementById('d-5').value;
document.getElementById('dev-2-3').innerHTML = document.getElementById('d-6').value;
document.getElementById('dev-3-1').innerHTML = document.getElementById('d-7').value;
document.getElementById('dev-3-2').innerHTML = document.getElementById('d-8').value;
document.getElementById('dev-3-3').innerHTML = document.getElementById('d-9').value;
document.getElementById('dev-4-1').innerHTML = document.getElementById('d-10').value;
document.getElementById('dev-4-2').innerHTML = document.getElementById('d-11').value;
document.getElementById('dev-4-3').innerHTML = document.getElementById('d-12').value;

	/* ======================= ПОЛУЧЕНИЕ ДАННЫХ ДЛЯ ТЕРМОМЕТРОВ ================== */
var t1 = document.getElementById('t1').value; 
var t2 = document.getElementById('t2').value;
var t3 = document.getElementById('t3').value;
var t4 = document.getElementById('t4').value;
var t5 = document.getElementById('t5').value;
var t6 = document.getElementById('t6').value;
var t7 = document.getElementById('t7').value;
var t8 = document.getElementById('t8').value;

	var options = {
		useEasing : true,
		useGrouping : true,
		separator : '',
		decimal : ',',
		prefix : '',
		suffix : ''
	};

	var dev11 = new CountUp("dev-1-1", 0, document.getElementById('d-1').value, 0, 0.5, options); dev11.start();

	/* ======================= АНИМАЦИЯ ОТОБРАЖЕНИЯ ПОКАЗАНИЙ БАРОМЕТРОВ ================== */
	
	var dev15 = new CountUp("dev15", 0, val1/10, 1, 19, options);
		dev15.start();
	var dev16 = new CountUp("dev16", 0, val2/10, 1, 4.5, options);
		dev16.start();
	var dev17 = new CountUp("dev17", 0, val3/10, 1, 15, options);
		dev17.start();
	var dev18 = new CountUp("dev18", 0, val4/10, 1, 4, options);
		dev18.start();
	var dev19 = new CountUp("dev19", 0, val5/10, 1, 15, options);
		dev19.start();
	var dev20 = new CountUp("dev20", 0, val6/10, 1, 5, options);
		dev20.start();	
	var dev21 = new CountUp("dev21", 0, val7/10, 1, 12, options);
		dev21.start();
	var dev22 = new CountUp("dev22", 0, val8/10, 1, 5, options);
		dev22.start();	
	
	/* ======================= АНИМАЦИЯ СТОЛБИКОВ ТЕРМОМЕТРОВ ================== */
	
	var t11 = new CountUp("t11", 0, t1, 0, 2, options);
		t11.start();
	var t12 = new CountUp("t12", 0, t2, 0, 2, options);
		t12.start();
	
	var t21 = new CountUp("t21", 0, t3, 0, 2, options);
		t21.start();
	var t22 = new CountUp("t22", 0, t4, 0, 2, options);
		t22.start();
	
	var t31 = new CountUp("t31", 0, t5, 0, 2, options);
		t31.start();
	var t32 = new CountUp("t32", 0, t6, 0, 2, options);
		t32.start();
	
	var t41 = new CountUp("t41", 0, t7, 0, 2, options);
		t41.start();
	var t42 = new CountUp("t42", 0, t8, 0, 2, options);
		t42.start();	
	
	/* ======================= АНИМАЦИЯ ПОДАЧИ ВОДЫ В ТРУБЫ ================== */

function water(w){ var w;
	for (var i = 1; i <= 5; i++) {
	  if (i == w) { //$('.b'+i).css('display','none');
		$('.w'+i).css('display','block'); 
	  }
	  else {$('.w'+i).css('display','none'); 
		//$('.b'+w).css('display','block');
	  }
	}
}
	
	/* ======================= АНИМАЦИЯ ПРОГРЕСС БАРОВ ================== */
	
	var v_11 = (document.getElementById('v-1-1').value*200)/1.5;  /* где 200- максимальная ширина прогресс бара в пикселях, 1,5 - это максимальное значение шкалы*/
	$('#p-level-1-1').animate({width:v_11+'px'},2000);	
	var v_12 = (document.getElementById('v-1-2').value*4000)/1;
	$('#p-level-1-2').animate({width:v_12+'px'},2000);	
	var v11 = new CountUp("val-1-1", 0, document.getElementById('v-1-1').value, 2, 2, options); v11.start();
	var v12 = new CountUp("val-1-2", 0, document.getElementById('v-1-2').value, 4, 2, options); v12.start();
	
	water(1);/* ======================= SECTION 1 DATA ======================= */
	$('#s1').click(function(){ water(1);
		$('#p-level-1-1').animate({width:'0px'},1);
		$('#p-level-1-2').animate({width:'0px'},1);
		var v_11 = (document.getElementById('v-1-1').value*200)/1.5; 
		$('#p-level-1-1').animate({width:v_11+'px'},2000);	
		var v_12 = (document.getElementById('v-1-2').value*4000)/1; 
		$('#p-level-1-2').animate({width:v_12+'px'},2000);	
			var v11 = new CountUp("val-1-1", 0, document.getElementById('v-1-1').value, 2, 2, options); v11.start();
			var v12 = new CountUp("val-1-2", 0, document.getElementById('v-1-2').value, 4, 2, options); v12.start();
	});
			/* ======================= SECTION 2 DATA ======================= */
	$('#s2').click(function(){ water(2);
		$('#r-val-1-1').animate({top:'0px',width:'0px',height:'0px'},1);
		$('#r-val-1-2').animate({top:'0px',width:'0px',height:'0px'},1);
		$('#r-val-1-3').animate({top:'0px',width:'0px',height:'0px'},1);
		$('#r-val-1-4').animate({top:'0px',width:'0px',height:'0px'},1);
		
		var rv11 = (document.getElementById('v-2-1').value*100)/20;
		$('#r-val-1-1').animate({top:'-'+rv11/2+'px',width:rv11+'px',height:rv11+'px'},1500);
		var r111 = new CountUp("val2-1", 0, document.getElementById('v-2-1').value, 2, 1.5, options);
		r111.start();	
		
		var rv12 = (document.getElementById('v-2-2').value*150)/1.5;
		$('#r-val-1-2').animate({top:'-'+rv12/2+'px',width:rv12+'px',height:rv12+'px'},1500);
		var r112 = new CountUp("val2-2", 0, document.getElementById('v-2-2').value, 2, 1.5, options);
		r112.start();	

		var rv13 = (document.getElementById('v-2-3').value*25);
		$('#r-val-1-3').animate({top:'-'+rv13/2+'px',width:rv13+'px',height:rv13+'px'},1500);
		var r113 = new CountUp("val2-3", 0, document.getElementById('v-2-3').value, 0, 1.5, options);
		r113.start();

		var rv14 = (document.getElementById('v-2-4').value*25);
		$('#r-val-1-4').animate({top:'-'+rv14/2+'px',width:rv14+'px',height:rv14+'px'},1500);
		var r114 = new CountUp("val2-4", 0, document.getElementById('v-2-4').value, 0, 1.5, options);
		r114.start();		
	});
			/* ======================= SECTION 3 DATA ======================= */
	$('#s3').click(function(){ water(3);
		$('#p-level-3-1').animate({width:'0px'},1);
		$('#p-level-3-2').animate({width:'0px'},1);
		$('#p-level-3-3').animate({width:'0px'},1);
		$('#p-level-3-4').animate({width:'0px'},1);
		$('#p-level-3-5').animate({width:'0px'},1);
		$('#level-3-1').animate({width:'0px'},1);
		$('#level-3-2').animate({width:'0px'},1);
		$('#level-3-3').animate({width:'0px'},1);
		$('#level-3-4').animate({width:'0px'},1);
		$('#level-3-5').animate({width:'0px'},1);		
		
		var rv31 = (document.getElementById('v-3-1').value*200)/0.3; 
		$('#p-level-3-1').animate({width:rv31+'px'},1500);
		var r311 = new CountUp("val3-1", 0, document.getElementById('v-3-1').value, 2, 1.5, options);
		r311.start();	
		
		var rv32 = (document.getElementById('v-3-2').value*200)/2; 
		$('#p-level-3-2').animate({width:rv32+'px'},1500);
		var r312 = new CountUp("val3-2", 0, document.getElementById('v-3-2').value, 2, 1.5, options);
		r312.start();	
		
		var rv33 = (document.getElementById('v-3-3').value*200)/(0.7*2); 
		$('#p-level-3-3').animate({width:rv33+'px'},1500);
		var r313 = new CountUp("val3-3", 0, document.getElementById('v-3-3').value, 2, 1.5, options);
		r313.start();
		
		var rv34 = (document.getElementById('v-3-4').value*2000)/(3*2); 
		$('#p-level-3-4').animate({width:rv34+'px'},1500);
		var r314 = new CountUp("val3-4", 0, document.getElementById('v-3-4').value, 3, 1.5, options);
		r314.start();	

		var rv35 = (document.getElementById('v-3-5').value*2000)/(2*2); 
		$('#p-level-3-5').animate({width:rv35+'px'},1500);
		var r315 = new CountUp("val3-5", 0, document.getElementById('v-3-5').value, 3, 1.5, options);
		r315.start();	

		var r31 = (document.getElementById('v3-1').value*200)/90; 
		$('#level-3-1').animate({width:r31+'px'},1500);
		var v311 = new CountUp("val-3-1", 0, document.getElementById('v3-1').value, 2, 1.5, options);
		v311.start();	
		var r32 = (document.getElementById('v3-2').value*200)/700; 
		$('#level-3-2').animate({width:r32+'px'},1500);
		var v312 = new CountUp("val-3-2", 0, document.getElementById('v3-2').value, 0, 1.5, options);
		v312.start();	
		var r33 = (document.getElementById('v3-3').value*200)/400; 
		$('#level-3-3').animate({width:r33+'px'},1500);
		var v313 = new CountUp("val-3-3", 0, document.getElementById('v3-3').value, 0, 1.5, options);
		v313.start();	 
		var r34 = (document.getElementById('v3-4').value*200)/1000; 
		$('#level-3-4').animate({width:r34+'px'},1500);
		var v314 = new CountUp("val-3-4", 0, document.getElementById('v3-4').value, 2, 1.5, options);
		v314.start();	
		var r35 = (document.getElementById('v3-5').value*200)/100; 
		$('#level-3-5').animate({width:r35+'px'},1500);
		var v315 = new CountUp("val-3-5", 0, document.getElementById('v3-5').value, 2, 1.5, options);
		v315.start();		
	});
 /* ======================= SECTION 4 DATA ======================= */
	$('#s4').click(function(){ water(4);
		$('#p-level-4-1').animate({width:'0px'},1);
		$('#p-level-4-2').animate({width:'0px'},1);
		$('#p-level-4-3').animate({width:'0px'},1);
		$('#p-level-4-4').animate({width:'16px',height:'0px',left:'120px'},1);
		$('#p-level-4-5').animate({width:'16px',height:'0px'},1);	
		
		var rv41 = (document.getElementById('v-4-1').value*200)/14; 
		$('#p-level-4-1').animate({width:rv41+'px'},1500);
		var r411 = new CountUp("val4-1", 0, document.getElementById('v-4-1').value, 2, 1.5, options);
		r411.start();	
		
		var rv42 = (document.getElementById('v-4-2').value*200)/1300; 
		$('#p-level-4-2').animate({width:rv42+'px'},1500);
		var r412 = new CountUp("val4-2", 0, document.getElementById('v-4-2').value, 0, 1.5, options);
		r412.start();	
		
		var rv43 = (document.getElementById('v-4-3').value*200)/0.4; 
		$('#p-level-4-3').animate({width:rv43+'px'},1500);
		var r413 = new CountUp("val4-3", 0, document.getElementById('v-4-3').value, 2, 1.5, options);
		r413.start();
		
		var rv44 = (document.getElementById('v-4-4').value*85)/5; //alert(rv44);
		$('#p-level-4-4').animate({width:'20px',height:rv44+'px',left:'116px'},1500);//.css('style','width:22px important;');
		var r414 = new CountUp("val4-4", 0, document.getElementById('v-4-4').value, 2, 1.5, options);
		r414.start();	

		var rv45 = (document.getElementById('v-4-5').value*85)/0.15; 
		$('#p-level-4-5').animate({width:'24px',height:rv45+'px'},1500);//.css('style','width:25px important;');
		var r415 = new CountUp("val4-5", 0, document.getElementById('v-4-5').value, 2, 1.5, options);
		r415.start();	
	});

	$('#s5').click(function(){ water(5); /* ======================= SECTION 5 DATA ======================= */
		$('#level-5-3').animate({width:'0px'},1);
		
		var r511 = new CountUp("val-5-1", 0, document.getElementById('v-5-1').value, 0, 1.5, options);
		r511.start();	
		if (document.getElementById('v-5-1').value>0) {
			$('#val-5-1').css('color','#D96877');
			$('#m5-1').css('color','#D96877');
		}
		
		var r512 = new CountUp("val-5-2", 0, document.getElementById('v-5-2').value, 0, 1.5, options);
		r512.start();	
		if (document.getElementById('v-5-2').value>0) {
			$('#val-5-2').css('color','#D96877');
			$('#m5-2').css('color','#D96877');
		}
		
		var rv53 = (document.getElementById('v-5-3').value*200)/100; 
		$('#level-5-3').animate({width:rv53+'px'},1500);
		var r513 = new CountUp("val-5-3", 0, document.getElementById('v-5-3').value, 0, 1.5, options);
		r513.start();
		if (document.getElementById('v-5-3').value>50) {
			$('#val-5-3').css('color','#D96877');
			$('#level-5-3').css('background','#D96877');
		}
	});	
	
	/* ======================= TEMPERATURE DATA ANIMATION ======================= */
 $('#p-1-1').animate({height:t1+'%'},1500).css('overflow','visible');
 $('#p-1-2').animate({height:t2+'%'},1500).css('overflow','visible'); 
 
 $('#p-2-1').animate({height:t3+'%'},1500).css('overflow','visible');
 $('#p-2-2').animate({height:t4+'%'},1500).css('overflow','visible');
 
 $('#p-3-1').animate({height:t5+'%'},1500).css('overflow','visible');
 $('#p-3-2').animate({height:t6+'%'},1500).css('overflow','visible');
 
 $('#p-4-1').animate({height:t7+'%'},1500).css('overflow','visible');
 $('#p-4-2').animate({height:t8+'%'},1500).css('overflow','visible'); 