var meter1,meter2,meter3,meter4,meter5,meter6,meter7,meter8,v1=0,v2=0,v3=0,v4=0,v5=0,v6=0,v7=0,v8=0,f1,f2,f3,f4,f5,f6,f7,f8,d1=1,d2=1,d3=1,d4=1,d5=1,d6=1,d7=1,d8=1;
var m1="#meter_1",m2="#meter_2",m3="#meter_3",m4="#meter_4",m5="#meter_5",m6="#meter_6",m7="#meter_7",m8="#meter_8";
var val1 = document.getElementById('p1').value*10; //alert(val1);
var val2 = document.getElementById('p2').value*10; //alert(val2);
var val3 = document.getElementById('p3').value*10; //alert(val3);
var val4 = document.getElementById('p4').value*10; //alert(val4);
var val5 = document.getElementById('p5').value*10;
var val6 = document.getElementById('p6').value*10;
var val7 = document.getElementById('p7').value*10;
var val8 = document.getElementById('p8').value*10;/* */
/*
  scale: { //шкала
    c: "black", //цвет
    w: 2, //толщина
    lm: { //большие деления
      s: 20, //шаг
      c: "black", //цвет
      w: 2, //толщина
      l: 3, //длина
      f: 8, //размер шрифта
      fc: "black" //цвет шрифта
    },
    sm: { //маленькие деления
      s: 10, //шаг
      c: "black", //цвет
      w: 1, //толщина
      l: 2 //длина
    }
*/

$(function() {
	meter1 = new JSGadget.Meter(m1,{
    title: "",gap:10,angle:216,min:0,max:120,
	scale: {c:"#e1987d",w: 0,
			  lm: {c:"#555",
				s: 10,
				w: 0,
				l: 1,
				f: 0
			  },
			 sm: {c:"#eeedea",
				s: 2,
				w: 0.5,
				l: 2
			  }
			},
    hand: {
	  c:"#4d4b47",w:6,l:40,l0:0,cr:5,f:"t"
    },
    font: {
      size: "bold 30"
    }
  });
   setInterval(function() {
    if (v1 <= val1-d1)
      f1 = true;
    else if (v1 >= val1)
      f1 = false;
    meter1.setVal(v1 += f1 ? 1 : -1);
  }, val1); 
});

$(function() {
	meter2 = new JSGadget.Meter(m2,{
    title: "",gap:10,angle:216,min:0,max:120,
	scale: {c:"#e1987d",w: 0,
			  lm: {c:"#555",
				s: 10,
				w: 0,
				l: 1,
				f: 0
			  },
			 sm: {c:"#eeedea",
				s: 2,
				w: 0.5,
				l: 2
			  }
			},
    hand: {
	  c:"#4d4b47",w:6,l:40,l0:0,cr:5,f:"t"
    },
    font: {
      size: "bold 30"
    }
  });
  setInterval(function() {
    if (v2 <= val2-d2)
      f2 = true;
    else if (v2 >= val2)
      f2 = false;
    meter2.setVal(v2 += f2 ? 1 : -1);
  }, val2); 
});

$(function() {
	meter3 = new JSGadget.Meter(m3,{
    title: "",gap:10,angle:216,min:0,max:120,
	scale: {c:"#e1987d",w: 0,
			  lm: {c:"#555",
				s: 10,
				w: 0,
				l: 1,
				f: 0
			  },
			 sm: {c:"#eeedea",
				s: 2,
				w: 0.5,
				l: 2
			  }
			},
    hand: {
	  c:"#4d4b47",w:6,l:40,l0:0,cr:5,f:"t"
    },
    font: {
      size: "bold 30"
    }
  });
  setInterval(function() {
    if (v3 <= val3-d3)
      f3 = true;
    else if (v3 >= val3)
      f3 = false;
    meter3.setVal(v3 += f3 ? 1 : -1);
  }, val3); 
});

$(function() {
	meter4 = new JSGadget.Meter(m4,{
    title: "",gap:10,angle:216,min:0,max:120,
	scale: {c:"#e1987d",w: 0,
			  lm: {c:"#555",
				s: 10,
				w: 0,
				l: 1,
				f: 0
			  },
			 sm: {c:"#eeedea",
				s: 2,
				w: 0.5,
				l: 2
			  }
			},
    hand: {
	  c:"#4d4b47",w:6,l:40,l0:0,cr:5,f:"t"
    },
    font: {
      size: "bold 30"
    }
  });
  setInterval(function() {
    if (v4 <= val4-d4)
      f4 = true;
    else if (v4 >= val4)
      f4 = false;
    meter4.setVal(v4 += f4 ? 1 : -1);
  }, val4); 
});

$(function() {
	meter5 = new JSGadget.Meter(m5,{
    title: "",gap:10,angle:216,min:0,max:120,
	scale: {c:"#e1987d",w: 0,
			  lm: {c:"#555",
				s: 10,
				w: 0,
				l: 1,
				f: 0
			  },
			 sm: {c:"#eeedea",
				s: 2,
				w: 0.5,
				l: 2
			  }
			},
    hand: {
	  c:"#4d4b47",w:6,l:40,l0:0,cr:5,f:"t"
    },
    font: {
      size: "bold 30"
    }
  });
  setInterval(function() {
    if (v5 <= val5-d5)
      f5 = true;
    else if (v5 >= val5)
      f5 = false;
    meter5.setVal(v5 += f5 ? 1 : -1);
  }, val5); 
});

$(function() {
	meter6 = new JSGadget.Meter(m6,{
    title: "",gap:10,angle:216,min:0,max:120,
	scale: {c:"#e1987d",w: 0,
			  lm: {c:"#555",
				s: 10,
				w: 0,
				l: 1,
				f: 0
			  },
			 sm: {c:"#eeedea",
				s: 2,
				w: 0.5,
				l: 2
			  }
			},
    hand: {
	  c:"#4d4b47",w:6,l:40,l0:0,cr:5,f:"t"
    },
    font: {
      size: "bold 30"
    }
  });
  setInterval(function() {
    if (v6 <= val6-d6)
      f6 = true;
    else if (v6 >= val6)
      f6 = false;
    meter6.setVal(v6 += f6 ? 1 : -1);
  }, val6); 
});

$(function() {
	meter7 = new JSGadget.Meter(m7,{
    title: "",gap:10,angle:216,min:0,max:120,
	scale: {c:"#e1987d",w: 0,
			  lm: {c:"#555",
				s: 10,
				w: 0,
				l: 1,
				f: 0
			  },
			 sm: {c:"#eeedea",
				s: 2,
				w: 0.5,
				l: 2
			  }
			},
    hand: {
	  c:"#4d4b47",w:6,l:40,l0:0,cr:5,f:"t"
    },
    font: {
      size: "bold 30"
    }
  });
  setInterval(function() {
    if (v7 <= val7-d7)
      f7 = true;
    else if (v7 >= val7)
      f7 = false;
    meter7.setVal(v7 += f7 ? 1 : -1);
  }, val7); 
});

$(function() {
	meter8 = new JSGadget.Meter(m8,{
    title: "",gap:10,angle:216,min:0,max:120,
	scale: {c:"#e1987d",w: 0,
			  lm: {c:"#555",
				s: 10,
				w: 0,
				l: 1,
				f: 0
			  },
			 sm: {c:"#eeedea",
				s: 2,
				w: 0.5,
				l: 2
			  }
			},
    hand: {
	  c:"#4d4b47",w:6,l:40,l0:0,cr:5,f:"t"
    },
    font: {
      size: "bold 30"
    }
  });
  setInterval(function() {
    if (v8 <= val8-d8)
      f8 = true;
    else if (v8 >= val8)
      f8 = false;
    meter8.setVal(v8 += f8 ? 1 : -1);
  }, val8); 
});